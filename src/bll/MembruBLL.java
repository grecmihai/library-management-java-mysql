package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.TelefonValidator;
import bll.validators.Validator;
import dao.MembruDAO;
import dao.MembruDAOImpl;
import model.ImprumutRezervare;
import model.Membru;

public class MembruBLL {
	private List<Validator<Membru>> validators;
	MembruDAO membruDAO;
	public MembruBLL(){
		validators = new ArrayList<>();
		validators.add(new EmailValidator());
		validators.add(new TelefonValidator());
		membruDAO = new MembruDAOImpl();
	}
	
	public Membru getMemberByName(String nume, String prenume, boolean obs){
		Membru membru = membruDAO.getMemberByName(nume, prenume,obs);
		if (membru == null){
			return null;
		}
		return membru;
	}
	
	public Membru getMemberByEmail(String email){
		Membru membru = membruDAO.getMemberByEmail(email);
		if (membru == null){
			throw new NoSuchElementException("The member with email "+ email +" was not found!");
		}
		return membru;
	}
	
	public String getPass(String email){
		String pass = membruDAO.getPass(email);
		if (pass == null){
			return null;
		}
		return pass;
	}
	
	public List<ImprumutRezervare> getImprumuturiCurente(String email){
		List<ImprumutRezervare> imprumuturiCurente = membruDAO.getImprumuturiCurente(email);
		if (imprumuturiCurente == null){
			throw new NoSuchElementException("The member with email "+ email +" was not found!");
		}
		return imprumuturiCurente;
	}
	
	public List<ImprumutRezervare> getRezervari(String email){
		List<ImprumutRezervare> rezervari = membruDAO.getImprumuturiCurente(email);
		if (rezervari == null){
			throw new NoSuchElementException("The member with email "+ email +" was not found!");
		}
		return rezervari;
	}
	
	public List<ImprumutRezervare> getIstoric(String email){
		List<ImprumutRezervare> istoric = membruDAO.getImprumuturiCurente(email);
		if (istoric == null){
			throw new NoSuchElementException("The member with email "+ email +" was not found!");
		}
		return istoric;
	}
	//<0 daca nu gaseste membrul,0 in caz de succes
	public int adaugarePenalizare(String nume, String prenume){
		return membruDAO.adaugarePenalizare(nume, prenume);
	}
	//<0 in caz ca nu gaseste membrul sau cartea, 0 in caz de succes
	public int prelungireCarte(String nume, String prenume,String titlu){
		return membruDAO.prelungireCarte(nume, prenume, titlu);
	}
	//-1 daca membrul sau cartea sunt prost date
	//-2 daca nu mai exista copie disponibila
	//-3 daca membrul nu are drept de imprumut
	//-4 daca membru are deja imprumutate 3 carti
	//0 la succes
	public int imprumutCarte(String nume, String prenume,String titlu){
		return membruDAO.imprumutCarte(nume, prenume, titlu);
	}
	//-1 daca informatiile sunt prost date
	//-2 daca informatiile sunt bune, dar membrul nu are cartea
	//0 la succes
	public int returnareCarte(String nume, String prenume,String titlu){
		return membruDAO.returnareCarte(nume, prenume, titlu);
	}
	//-1 daca numele nu e bun
	//-2 daca membrul nu are de fapt amenda de platit
	//0 in caz de succes
	public int platireAmenda(String nume, String prenume){
		return membruDAO.platireAmenda(nume, prenume);
	}
	public void actualizareRezervari(){
		membruDAO.actualizareRezervari();
	}
	
	public List<Membru> getAllMembers(boolean obs){
		List<Membru> members = membruDAO.getAllMembers(obs);
		if (null == members){
			throw new NoSuchElementException("Something went wrong when trying to get all the members.");
		}
		return members;
	}
	
	//-1 daca membrul sau cartea sunt prost date
		//-2 daca nu mai exista copie disponibila
		//-3 daca membrul nu are drept de rezervare
		//-4 daca membru are deja rezervate 3 carti
		// 0 la succes
		public int rezervareCarte(String nume, String prenume,String titlu){
			return membruDAO.rezervareCarte(nume, prenume, titlu);
		}
		public int insert(Membru membru){
			for (Validator<Membru> v : validators){
				v.validate(membru);
			}
			return membruDAO.insert(membru);
		}
		
		public boolean update(Membru membru , String cust){
			for (Validator<Membru> v : validators){
				v.validate(membru);
			}
			boolean isValid = membruDAO.update(membru, cust);
			if (!isValid)
				throw new NoSuchElementException("The member with name "+cust+" was not found!!!");
			return isValid;
		}

}
