package bll;

import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.AnAparitieValidator;
import bll.validators.Validator;
import dao.CarteDAO;
import dao.CarteDAOImpl;
import model.Carte;

public class CarteBLL {
	private Validator<Carte> validator;
	CarteDAO carteDAO;
	public CarteBLL(){
		validator = new AnAparitieValidator();
		carteDAO = new CarteDAOImpl();
	}
	
	public List<String> getAuthors(String titlu){
		List<String> authors = carteDAO.getAuthors(titlu);
		if (null == authors){
			throw new NoSuchElementException("The book with title "+ titlu +" was not found!");
		}
		return authors;
	}
	
	public List<String> getGenres(String titlu){
		List<String> genres = carteDAO.getAuthors(titlu);
		if (null == genres){
			throw new NoSuchElementException("The book with title "+ titlu +" was not found!");
		}
		return genres;
	}
	
	public Carte findByISBN(String ISBN){
		Carte carte = carteDAO.findByISBN(ISBN);
		if (null == carte){
			throw new NoSuchElementException("The book with ISBN "+ ISBN +" was not found!");
		}
		return carte;
	}
	
	public Carte findByTitle(String titlu){
		Carte carte = carteDAO.findByTitle(titlu);
		if (null == carte){
			return null;
		}
		return carte;
	}
	
	public List<Carte> findBooksByAuthor(String autor){
		List<Carte> carti = carteDAO.findBooksByAuthor(autor);
		if (null == carti){
			throw new NoSuchElementException("The author "+ autor +" was not found!");
		}
		return carti;
	}
	
	public List<Carte> findBooksByGenre(String gen){
		List<Carte> carti = carteDAO.findBooksByGenre(gen);
		if (null == carti){
			throw new NoSuchElementException("The genre "+ gen +" was not found!");
		}
		return carti;
	}
	
	public List<Carte> findBooksByTitle(String titlu){
		List<Carte> carti = carteDAO.findBooksByTitle(titlu);
		if (null == carti){
			throw new NoSuchElementException("The title "+ titlu +" was not found!");
		}
		return carti;
	}
	
	public void insert(Carte carte){
		validator.validate(carte);
		carteDAO.insert(carte);
	}
}
