package bll.validators;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;

import model.Membru;

public class TelefonValidator implements Validator<Membru>{
	public void validate(Membru t){
		char one = t.getNrTelefon().charAt(0);
		char two = t.getNrTelefon().charAt(1);
		if (one != '0' || two != '7' || t.getNrTelefon().length() != 10){
			JOptionPane.showMessageDialog(null, "Invalid phone number" , "",JOptionPane.PLAIN_MESSAGE);
			throw new IllegalArgumentException("Invalid phone number");
		}
		if (!StringUtils.isNumeric(t.getNrTelefon())){
			JOptionPane.showMessageDialog(null, "Invalid phone number" , "",JOptionPane.PLAIN_MESSAGE);
			throw new IllegalArgumentException("Phone number not numeric");
		}
	}
}
