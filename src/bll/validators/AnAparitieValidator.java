package bll.validators;

import javax.swing.JOptionPane;

import model.Carte;

public class AnAparitieValidator implements Validator<Carte>{

	public void validate(Carte t) {
		if (t.getAnAparitie() < 1000 || t.getAnAparitie() > 9999){
			JOptionPane.showMessageDialog(null,"Invalid year","",JOptionPane.PLAIN_MESSAGE);
			throw new IllegalArgumentException("Invalid year");
		}
	}

}
