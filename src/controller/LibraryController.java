package controller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import bll.CarteBLL;
import bll.MembruBLL;
import controller.LibraryController.ActualRezListener;
import controller.LibraryController.AdPenListener;
import controller.LibraryController.AdaugarePenalizarePageListener;
import controller.LibraryController.CaImpListener;
import controller.LibraryController.CatListener;
import controller.LibraryController.ImCarListener;
import controller.LibraryController.ImprumutCartePageListener;
import controller.LibraryController.ImprumuturiListener;
import controller.LibraryController.IstoricListener;
import controller.LibraryController.LoginListener;
import controller.LibraryController.PlAmeListener;
import controller.LibraryController.PlatireAmendaPageListener;
import controller.LibraryController.PrCarListener;
import controller.LibraryController.PrelungireCartePageListener;
import controller.LibraryController.ReCarListener;
import controller.LibraryController.ReturnareCartePageListener;
import controller.LibraryController.RezervaListener;
import controller.LibraryController.RezervariListener;
import controller.LibraryController.SearchbtnListener;
import model.Adresa;
import model.Carte;
import model.Membru;
import views.LibraryView;

public class LibraryController {
	private LibraryView libraryView;
	private MembruBLL membruBLL;
	private CarteBLL carteBLL;
	private String user;
	
	public LibraryController(LibraryView view){
		libraryView = view;
		membruBLL = new MembruBLL();
		carteBLL = new CarteBLL();
		
		libraryView.getMain().addLoginButtonListener(new LoginButtonListener());
		libraryView.getLog().addLoginListener(new LoginListener());
		libraryView.getMemberPage().addImprumuturiListener(new ImprumuturiListener());
		libraryView.getMemberPage().addIstoricListener(new IstoricListener());
		libraryView.getMemberPage().addRezervariListener(new RezervariListener());
		libraryView.getMemberPage().addRezervaListener(new RezervaListener());
		libraryView.getMemberPage().addCatalogListener(new CatListener());
		libraryView.getRezervaPage().addRezervaListener(new RezervListener());
		libraryView.getCatalogPage().addSearchbtnListener(new SearchbtnListener());
		libraryView.getAdminPage().addAdPenListener(new AdPenListener());
		libraryView.getAdaugarePenalizarePage().addAdaugarePenalizarePage(new AdaugarePenalizarePageListener());
		libraryView.getAdminPage().addCatListener(new CatListener());
		libraryView.getAdminPage().AddActualRezListener(new ActualRezListener());
		libraryView.getAdminPage().addCaImpListener(new CaImpListener());
		libraryView.getAdminPage().addPlAmeListener(new PlAmeListener());
		libraryView.getPlatireAmendaPage().addPlatireAmendaPageListener(new PlatireAmendaPageListener());
		libraryView.getAdminPage().addReCarListener(new ReCarListener());
		libraryView.getReturnareCartePage().addReturnareCartePageListener(new ReturnareCartePageListener());
		libraryView.getAdminPage().addPrCarListener(new PrCarListener());
		libraryView.getPrelungireCartePage().addPrelungireCartePageListener(new PrelungireCartePageListener());
		libraryView.getAdminPage().addImCarListener(new ImCarListener());
		libraryView.getImprumutCartePage().addImprumutCartePageListener(new ImprumutCartePageListener());
		libraryView.getAdminPage().addAddMemberListener(new AddMemberListener());
		libraryView.getAdaugareMembruPage().addExecuteButtonListener(new AdaugareMembruListener());
		libraryView.getAdminPage().addAddBookListener(new AddBookListener());
		libraryView.getAdaugareCartePage().addExecuteButtonListener(new AdaugareCarteListener());
		libraryView.getAdminPage().addUpdateMemberListener(new UpdateMemberListener());
		libraryView.getEditareMembruPage().addExecuteButtonListener(new EditareMembruPageListener());
	


	}
	private static JComboBox createComboBox(List<Membru> members){
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		for(Membru m : members){
			model.addElement(m.getNume() + " " + m.getPrenume());
		}
		JComboBox comboBox = new JComboBox(model);
		comboBox.setSelectedIndex(0);
		comboBox.setSelectedItem(0);
		return comboBox;
	}
	
	class UpdateMemberListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			List<Membru> members = membruBLL.getAllMembers(false);
			libraryView.getEditareMembruPage().setComboBox(createComboBox(members));
			libraryView.getEditareMembruPage().setVisible(true);
			String nume = String.valueOf(libraryView.getEditareMembruPage().getComboBox().getSelectedItem());
			Membru membru = membruBLL.getMemberByName(nume.split(" ")[0], nume.split(" ")[1],false);
			libraryView.getEditareMembruPage().setTexts(membru.getNume(), membru.getEmail(), membru.getNrTelefon(),
					membru.getAdresa().getStrada(), String.valueOf(membru.getAdresa().getNumar()),
					membru.getAdresa().getBloc(), String.valueOf(membru.getAdresa().getApartament()));
			libraryView.getEditareMembruPage().addComboBoxListener(new ComboBoxListener(), 
					libraryView.getEditareMembruPage().getComboBox());
		}
	}
	class ComboBoxListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String nume = String.valueOf(libraryView.getEditareMembruPage().getComboBox().getSelectedItem());
			Membru membru = membruBLL.getMemberByName(nume.split(" ")[0], nume.split(" ")[1],false);
			libraryView.getEditareMembruPage().setTexts(membru.getNume(), membru.getEmail(), membru.getNrTelefon(),
					membru.getAdresa().getStrada(), String.valueOf(membru.getAdresa().getNumar()),
					membru.getAdresa().getBloc(), String.valueOf(membru.getAdresa().getApartament()));
		}
	}
	class EditareMembruPageListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String nume = String.valueOf(libraryView.getEditareMembruPage().getComboBox().getSelectedItem());
			if (libraryView.getEditareMembruPage().getNumeT().getText().isEmpty() || 
					libraryView.getEditareMembruPage().getEmailT().getText().isEmpty() ||
					libraryView.getEditareMembruPage().getTelefonT().getText().isEmpty() ||
					libraryView.getEditareMembruPage().getStradaT().getText().isEmpty() ||
					libraryView.getEditareMembruPage().getNumarT().getText().isEmpty() ){
				JOptionPane.showMessageDialog(null, "Fill all the informations" , "",JOptionPane.PLAIN_MESSAGE);
			}
			else{
				Adresa adresa;
				if (libraryView.getEditareMembruPage().getBlocT().getText().isEmpty()){
					adresa = new Adresa(libraryView.getEditareMembruPage().getStradaT().getText(),
							Integer.parseInt(libraryView.getEditareMembruPage().getNumarT().getText()));
				}
				else{
					adresa = new Adresa(libraryView.getEditareMembruPage().getStradaT().getText(),
							Integer.parseInt(libraryView.getEditareMembruPage().getNumarT().getText()),
							libraryView.getEditareMembruPage().getStradaT().getText(),
							Integer.parseInt(libraryView.getEditareMembruPage().getNumarT().getText()));
				}
				Membru membru = membruBLL.getMemberByName(nume.split(" ")[0], nume.split(" ")[1],false);
				membru.setNume(libraryView.getEditareMembruPage().getNumeT().getText());
				membru.setEmail(libraryView.getEditareMembruPage().getEmailT().getText());
				membru.setNrTelefon(libraryView.getEditareMembruPage().getTelefonT().getText());
				membru.setAdresa(adresa);
				membruBLL.update(membru, nume);
				JOptionPane.showMessageDialog(null, "Succes!" , "",JOptionPane.PLAIN_MESSAGE);
			}
		}
	}
	class AddMemberListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getAdaugareMembruPage().setVisible(true);
		}
	}
	class AddBookListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getAdaugareCartePage().setVisible(true);
		}
	}
	class ImCarListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getImprumutCartePage().getNume().setText("");
			libraryView.getImprumutCartePage().getPrenume().setText("");
			libraryView.getImprumutCartePage().getTitlu().setText("");
			libraryView.getImprumutCartePage().setVisible(true);
			libraryView.getImprumutCartePage().getMesaj().setVisible(false);
		}
	}
	
	class PrCarListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getPrelungireCartePage().getNume().setText("");
			libraryView.getPrelungireCartePage().getPrenume().setText("");
			libraryView.getPrelungireCartePage().getTitlu().setText("");
			libraryView.getPrelungireCartePage().setVisible(true);
			libraryView.getPrelungireCartePage().getMesaj().setVisible(false);
		}
	}
	class LoginButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getLog().getTxuser().setText("");
			libraryView.getLog().getPass().setText("");
			libraryView.getLog().setVisible(true);
			libraryView.getLog().getMesaj().setVisible(false);
			
		}
	}
	class ReCarListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getReturnareCartePage().getNume().setText("");
			libraryView.getReturnareCartePage().getPrenume().setText("");
			libraryView.getReturnareCartePage().getTitlu().setText("");
			libraryView.getReturnareCartePage().setVisible(true);
			libraryView.getReturnareCartePage().getMesaj().setVisible(false);
		}
	}
	class PlAmeListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getPlatireAmendaPage().getNume().setText("");
			libraryView.getPlatireAmendaPage().getPrenume().setText("");
			libraryView.getPlatireAmendaPage().setVisible(true);
			libraryView.getPlatireAmendaPage().getMesaj().setVisible(false);
		}
	}
	class LoginListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String cont, parola;
			char[] helper;
			cont = libraryView.getLog().getTxuser().getText();
			helper = libraryView.getLog().getPass().getPassword();
			parola = String.valueOf(helper);
			user = cont;
			String pass = membruBLL.getPass(cont);
			if (pass != null){
				if (cont.equals("admin")){
					libraryView.setMainPage(libraryView.getMainPageFactory().getMainPage("admin", libraryView.getAdminPage(), libraryView.getMemberPage()));
					libraryView.getMainPage().setVisibility(true);
					libraryView.getMain().setVisible(false);
					libraryView.getLog().setVisible(false);
				}
				else{
					if (parola.equals(pass)){
						libraryView.setMainPage(libraryView.getMainPageFactory().getMainPage("member", libraryView.getAdminPage(), libraryView.getMemberPage()));
						libraryView.getMainPage().setVisibility(true);
						libraryView.getMain().setVisible(false);
						libraryView.getLog().setVisible(false);
						Membru membru = membruBLL.getMemberByEmail(cont);
						int nrCarti = membru.getImprumuturi().size();
						String[] coloane = {"Titlu" , "Data"};
						Object[][] informatii = new Object[nrCarti][2];
						int i=0;
						while(i < membru.getImprumuturi().size()){
							String helper1 = String.format("          %s", membru.getImprumuturi().get(i).getCarte());
							informatii[i][0] = helper1;
							informatii[i][1] = membru.getImprumuturi().get(i).getDataValabilitate();
							i++;
						}
						JTable table = new JTable(informatii , coloane);
						JScrollPane scrollPane = new JScrollPane(table);
						Color color = new Color(255 , 255 , 134);
						table.setBackground(color);
						table.setFillsViewportHeight(true);
						table.setEnabled(false);
						table.setShowGrid(false);
						table.setRowHeight(table.getRowHeight() + 250);
						table.setFont(new Font("Serif" , Font.BOLD , 35));
						table.getColumnModel().getColumn(0).setPreferredWidth(450);
						libraryView.getMemberPage().getAfisaj().removeAll();
						libraryView.getMemberPage().getAfisaj().add(table , BorderLayout.CENTER);
						libraryView.getMemberPage().getAfisaj().setBackground(color);
						libraryView.getMemberPage().getAfisaj().revalidate();
					}
					else{
						libraryView.getLog().getMesaj().setVisible(true);
					}
				}
			}
			else{
				libraryView.getLog().getMesaj().setVisible(true);
			}
		}
	}
	class ImprumuturiListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			Membru membru = membruBLL.getMemberByEmail(user);
			int nrCarti = membru.getImprumuturi().size();
			String[] coloane = {"Titlu" , "Data"};
			Object[][] informatii = new Object[nrCarti][2];
			int i=0;
			while(i < membru.getImprumuturi().size()){
				String helper1 = String.format("          %s", membru.getImprumuturi().get(i).getCarte());
				informatii[i][0] = helper1;
				informatii[i][1] = membru.getImprumuturi().get(i).getDataValabilitate();
				i++;
			}
			JTable table = new JTable(informatii , coloane);
			JScrollPane scrollPane = new JScrollPane(table);
			Color color = new Color(255 , 255 , 134);
			table.setBackground(color);
			table.setFillsViewportHeight(true);
			table.setEnabled(false);
			table.setShowGrid(false);
			table.setRowHeight(table.getRowHeight() + 250);
			table.setFont(new Font("Serif" , Font.BOLD , 35));
			table.getColumnModel().getColumn(0).setPreferredWidth(450);
			libraryView.getMemberPage().getAfisaj().removeAll();
			libraryView.getMemberPage().getAfisaj().add(table , BorderLayout.CENTER);
			libraryView.getMemberPage().getAfisaj().setBackground(color);
			libraryView.getMemberPage().getAfisaj().revalidate();
		}
		
	}
	class IstoricListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			Membru membru = membruBLL.getMemberByEmail(user);
			int nrCarti = membru.getIstoric().size();
			String[] coloane = {"Titlu" , "Data preluare" , "Data predare"};
			Object[][] informatii = new Object[nrCarti][3];
			int i = 0;
			while (i<membru.getIstoric().size()){
				informatii[i][0] = membru.getIstoric().get(i).getCarte();
				informatii[i][1] = membru.getIstoric().get(i).getDataImprumutului();
				informatii[i][2] = membru.getIstoric().get(i).getDataValabilitate();
				i++;
			}
			JTable table = new JTable(informatii , coloane);
			JScrollPane scrollPane = new JScrollPane(table);
			Color color = new Color(255 , 255 , 134);
			table.setBackground(color);
			table.setFillsViewportHeight(true);
			table.setEnabled(false);
			table.setFont(new Font("Serif" , Font.BOLD , 15));
			libraryView.getMemberPage().getAfisaj().removeAll();
			libraryView.getMemberPage().getAfisaj().add(table.getTableHeader() , BorderLayout.NORTH);
			libraryView.getMemberPage().getAfisaj().add(table , BorderLayout.CENTER);
			libraryView.getMemberPage().getAfisaj().setBackground(color);
			libraryView.getMemberPage().getAfisaj().revalidate();
			
		}
		
	}
	class RezervariListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			Membru membru = membruBLL.getMemberByEmail(user);
			int nrCarti = membru.getRezervari().size();
			String[] coloane = {"Titlu" , "Data rezervare" , "Data valabilitate"};
			Object[][] informatii = new Object[nrCarti][3];
			int i=0;
			while(i<membru.getRezervari().size()){
				informatii[i][0] = membru.getRezervari().get(i).getCarte();
				informatii[i][1] = membru.getRezervari().get(i).getDataImprumutului();
				informatii[i][2] = membru.getRezervari().get(i).getDataValabilitate();
				i++;
			}
			JTable table = new JTable(informatii , coloane);
			JScrollPane scrollPane = new JScrollPane(table);
			Color color = new Color(255 , 255 , 134);
			table.setBackground(color);
			table.setFillsViewportHeight(true);
			table.setEnabled(false);
			table.setFont(new Font("Serif" , Font.BOLD , 15));
			libraryView.getMemberPage().getAfisaj().removeAll();
			libraryView.getMemberPage().getAfisaj().add(table.getTableHeader() , BorderLayout.NORTH);
			libraryView.getMemberPage().getAfisaj().add(table , BorderLayout.CENTER);
			libraryView.getMemberPage().getAfisaj().setBackground(color);
			libraryView.getMemberPage().getAfisaj().revalidate();
		}
	}
	
	class RezervaListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getRezervaPage().getTitlu().setText("");
			libraryView.getRezervaPage().setVisible(true);
			libraryView.getRezervaPage().getMesaj().setVisible(false);
		}
	}
	class CatListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getCatalogPage().setVisible(true);
		}
	}
	class RezervListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String titlu;
			Membru membru = membruBLL.getMemberByEmail(user);
			titlu = libraryView.getRezervaPage().getTitlu().getText();
			Carte carte = carteBLL.findByTitle(titlu);
			if (carte != null){
				int res = membruBLL.rezervareCarte(membru.getNume(), membru.getPrenume(), titlu);
				switch(res){
				case -1:{
					libraryView.getRezervaPage().getMesaj().setText("Carte inexistenta.");
					libraryView.getRezervaPage().getMesaj().setForeground(Color.red);
					libraryView.getRezervaPage().getMesaj().setVisible(true);
					break;
				}
				case -2:{
					libraryView.getRezervaPage().getMesaj().setText("Cartea nu este la raft.");
					libraryView.getRezervaPage().getMesaj().setForeground(Color.BLACK);
					libraryView.getRezervaPage().getMesaj().setVisible(true);
					break;
				}
				case -3:{
					libraryView.getRezervaPage().getMesaj().setText("Membru suspendat.");
					libraryView.getRezervaPage().getMesaj().setForeground(Color.red);
					libraryView.getRezervaPage().getMesaj().setVisible(true);
					break;
				}
				case -4:{
					libraryView.getRezervaPage().getMesaj().setText("S-a atins nr maxim de rezervari.");
					libraryView.getRezervaPage().getMesaj().setForeground(Color.BLACK);
					libraryView.getRezervaPage().getMesaj().setVisible(true);
					break;
				}
				default:{
					libraryView.getRezervaPage().getMesaj().setText("Carte rezervata.");
					libraryView.getRezervaPage().getMesaj().setForeground(Color.GREEN);
					libraryView.getRezervaPage().getMesaj().setVisible(true);
				}
				}
				
			}
			else{
				libraryView.getRezervaPage().getMesaj().setText("Carte inexistenta.");
				libraryView.getRezervaPage().getMesaj().setForeground(Color.red);
				libraryView.getRezervaPage().getMesaj().setVisible(true);
			}
		}
	}
	class SearchbtnListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String cautat = libraryView.getCatalogPage().getSearchtxt().getText();
			List<Carte> carti = carteBLL.findBooksByTitle(cautat);
			List<Carte> cartiAutor = carteBLL.findBooksByAuthor(cautat);
			List<Carte> cartiGen = carteBLL.findBooksByGenre(cautat);
			BookFactory bookFactory = new BookFactory();
			for(Carte carte : carti){
				bookFactory.insertBook(carte);
			}
			for(Carte carte : cartiAutor){
				bookFactory.insertBook(carte);
			}
			for(Carte carte : cartiGen){
				bookFactory.insertBook(carte);
			}
			int number = bookFactory.getBookMap().size();
			Object[][] date = new Object[number][7];
			Iterator it = bookFactory.getBookMap().entrySet().iterator();
			int i = 0;
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println(pair.getKey() + " = " + pair.getValue());
		        Carte carte = (Carte)pair.getValue();
		        date[i][0] = carte.getTitlu();
				date[i][1] = carte.getAutori().get(0).split(" ")[0];
				date[i][2] = carte.getAutori().get(0).split(" ")[1];
				date[i][3] = carte.getGen().get(0);
				date[i][4] = carte.getLimba();
				date[i][5] = carte.getCod();
 				date[i][6] = carte.getNrCopiiDisponibile();
				i++;
		        it.remove();
		    }
			libraryView.getCatalogPage().getTablePanel().removeAll();
			libraryView.getCatalogPage().setData(date);
			libraryView.getCatalogPage().setTable(new JTable(libraryView.getCatalogPage().getData() , libraryView.getCatalogPage().getColumns()));
			libraryView.getCatalogPage().setScrollPane(new JScrollPane(libraryView.getCatalogPage().getTable()));
			libraryView.getCatalogPage().getTable().setFillsViewportHeight(true);
			libraryView.getCatalogPage().getTablePanel().add(libraryView.getCatalogPage().getTable().getTableHeader() , BorderLayout.NORTH);
			libraryView.getCatalogPage().getTablePanel().add(libraryView.getCatalogPage().getTable() , BorderLayout.CENTER);
			libraryView.getCatalogPage().getTablePanel().revalidate();
		}
		
	}
	class AdPenListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			libraryView.getAdaugarePenalizarePage().getNume().setText("");
			libraryView.getAdaugarePenalizarePage().getPrenume().setText("");
			libraryView.getAdaugarePenalizarePage().setVisible(true);
			libraryView.getAdaugarePenalizarePage().getMesaj().setVisible(false);
		}
	}
	
	class AdaugarePenalizarePageListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String nume , prenume;
			nume = libraryView.getAdaugarePenalizarePage().getNume().getText();
			prenume = libraryView.getAdaugarePenalizarePage().getPrenume().getText();
			Membru membru = membruBLL.getMemberByName(nume, prenume,false);
			if (membru != null){
				membruBLL.adaugarePenalizare(nume, prenume);
				libraryView.getAdaugarePenalizarePage().getMesaj().setText("Succes!!!");
				libraryView.getAdaugarePenalizarePage().getMesaj().setForeground(Color.green);
				libraryView.getAdaugarePenalizarePage().getMesaj().setVisible(true);
			}
			else {
				libraryView.getAdaugarePenalizarePage().getMesaj().setText("Nume sau prenume gresit");
				libraryView.getAdaugarePenalizarePage().getMesaj().setForeground(Color.RED);
				libraryView.getAdaugarePenalizarePage().getMesaj().setVisible(true);
			}
		}
	}
	class ActualRezListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			membruBLL.actualizareRezervari();
			JOptionPane.showMessageDialog(null, "Rezervarile expirate au fost sterse." , "" , JOptionPane.PLAIN_MESSAGE);
		}
	}
	class CaImpListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			List<Membru> membrii = membruBLL.getAllMembers(false);
			int nr = 0;
			for (Membru m : membrii){
				if(m != null){
					if (m.getImprumuturi() != null){
						nr += m.getImprumuturi().size();
					}
				}
			}
			Object[][] date = new Object[nr][4];
			int i = 0;
			int j = 0;
			for (Membru m:membrii){
				if (m != null){
					if (m.getImprumuturi() != null){
						j = 0;
						while(j < m.getImprumuturi().size()){
							date[i][0] = m.getNume();
							date[i][1] = m.getPrenume();
							date[i][2] = m.getImprumuturi().get(j).getCarte();
							date[i][3] = m.getImprumuturi().get(j).getDataValabilitate();
							i++;
							j++;
						}
					}
				}
			}
			
			libraryView.getCartiImprumutatePage().getPanel().removeAll();
			libraryView.getCartiImprumutatePage().setData(date);
			libraryView.getCartiImprumutatePage().setTable(new JTable(libraryView.getCartiImprumutatePage().getData() , libraryView.getCartiImprumutatePage().getColumns()));
			libraryView.getCartiImprumutatePage().setScrollPane(new JScrollPane(libraryView.getCartiImprumutatePage().getTable()));
			libraryView.getCartiImprumutatePage().getTable().setFillsViewportHeight(true);
			libraryView.getCartiImprumutatePage().getPanel().add(libraryView.getCartiImprumutatePage().getTable().getTableHeader() , BorderLayout.NORTH);
			libraryView.getCartiImprumutatePage().getPanel().add(libraryView.getCartiImprumutatePage().getTable() , BorderLayout.CENTER);
			libraryView.getCartiImprumutatePage().getPanel().revalidate();
			libraryView.getCartiImprumutatePage().setVisible(true);
		}
	}
	class PlatireAmendaPageListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String nume , prenume;
			nume = libraryView.getPlatireAmendaPage().getNume().getText();
			prenume = libraryView.getPlatireAmendaPage().getPrenume().getText();
			Membru membru = membruBLL.getMemberByName(nume, prenume, false);
			if (membru != null){
				int i = membruBLL.platireAmenda(nume, prenume);
				if (i == -2){
					libraryView.getPlatireAmendaPage().getMesaj().setText("Membrul nu este suspendat");
					libraryView.getPlatireAmendaPage().getMesaj().setForeground(Color.BLACK);
					libraryView.getPlatireAmendaPage().getMesaj().setVisible(true);
				}
				else{
					libraryView.getPlatireAmendaPage().getMesaj().setText("Succes!!!");
					libraryView.getPlatireAmendaPage().getMesaj().setForeground(Color.GREEN);
					libraryView.getPlatireAmendaPage().getMesaj().setVisible(true);
				}
			}
			else{
				libraryView.getPlatireAmendaPage().getMesaj().setText("Date introduse gresit");
				libraryView.getPlatireAmendaPage().getMesaj().setForeground(Color.red);
				libraryView.getPlatireAmendaPage().getMesaj().setVisible(true);
			}
			
		}
	}
	class ReturnareCartePageListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String nume , prenume , titlu;
			nume = libraryView.getReturnareCartePage().getNume().getText();
			prenume = libraryView.getReturnareCartePage().getPrenume().getText();
			titlu = libraryView.getReturnareCartePage().getTitlu().getText();
			Membru membru = membruBLL.getMemberByName(nume, prenume,false);
			Carte carte = carteBLL.findByTitle(titlu);
			if (membru != null && carte != null){
				int i = membruBLL.returnareCarte(nume, prenume, titlu);
				if (i == -2){
					libraryView.getReturnareCartePage().getMesaj().setText("Membrul nu are cartea");
					libraryView.getReturnareCartePage().getMesaj().setForeground(Color.BLACK);
					libraryView.getReturnareCartePage().getMesaj().setVisible(true);
				}
				else{
					libraryView.getReturnareCartePage().getMesaj().setText("Succes!!!");
					libraryView.getReturnareCartePage().getMesaj().setForeground(Color.GREEN);
					libraryView.getReturnareCartePage().getMesaj().setVisible(true);
				}
			}
			else{
				libraryView.getReturnareCartePage().getMesaj().setText("Date introduse gresit");
				libraryView.getReturnareCartePage().getMesaj().setForeground(Color.red);
				libraryView.getReturnareCartePage().getMesaj().setVisible(true);
			}
			
		}
	}
	
	class PrelungireCartePageListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String nume , prenume , titlu;
			nume = libraryView.getPrelungireCartePage().getNume().getText();
			prenume = libraryView.getPrelungireCartePage().getPrenume().getText();
			titlu = libraryView.getPrelungireCartePage().getTitlu().getText();
			Membru membru = membruBLL.getMemberByName(nume, prenume,false);
			Carte carte = carteBLL.findByTitle(titlu);
			if (membru != null && carte != null){
				membruBLL.prelungireCarte(nume, prenume, titlu);
				libraryView.getPrelungireCartePage().getMesaj().setText("Succes!!!");
				libraryView.getPrelungireCartePage().getMesaj().setForeground(Color.green);
				libraryView.getPrelungireCartePage().getMesaj().setVisible(true);
			}
			else{
				libraryView.getPrelungireCartePage().getMesaj().setText("Date introduse gresit");
				libraryView.getPrelungireCartePage().getMesaj().setForeground(Color.RED);
				libraryView.getPrelungireCartePage().getMesaj().setVisible(true);
			}
		}
	}
	class ImprumutCartePageListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String nume , prenume , titlu;
			nume = libraryView.getImprumutCartePage().getNume().getText();
			prenume = libraryView.getImprumutCartePage().getPrenume().getText();
			titlu = libraryView.getImprumutCartePage().getTitlu().getText();
			Membru membru = membruBLL.getMemberByName(nume, prenume, false);
			Carte carte = carteBLL.findByTitle(titlu);
			if (membru != null && carte != null){
				int i = membruBLL.imprumutCarte(nume, prenume, titlu);
				if (i == -2){
					libraryView.getImprumutCartePage().getMesaj().setText("Fara copie");
					libraryView.getImprumutCartePage().getMesaj().setForeground(Color.red);
					libraryView.getImprumutCartePage().getMesaj().setVisible(true);
				}
				else{
					if (i == -3){
						libraryView.getImprumutCartePage().getMesaj().setText("Fara drept");
						libraryView.getImprumutCartePage().getMesaj().setForeground(Color.red);
						libraryView.getImprumutCartePage().getMesaj().setVisible(true);
					}
					else{
						if (i == -4){
							libraryView.getImprumutCartePage().getMesaj().setText("3 carti deja");
							libraryView.getImprumutCartePage().getMesaj().setForeground(Color.red);
							libraryView.getImprumutCartePage().getMesaj().setVisible(true);
						}
						else{
							libraryView.getImprumutCartePage().getMesaj().setText("Succes!!!");
							libraryView.getImprumutCartePage().getMesaj().setForeground(Color.green);
							libraryView.getImprumutCartePage().getMesaj().setVisible(true);
						}
					}
				}
			}
			else{
				libraryView.getImprumutCartePage().getMesaj().setText("Date introduse incorect");
				libraryView.getImprumutCartePage().getMesaj().setForeground(Color.red);
				libraryView.getImprumutCartePage().getMesaj().setVisible(true);
			}
		}
	}
	class AdaugareMembruListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if (libraryView.getAdaugareMembruPage().getNumeT().getText().isEmpty() ||
				libraryView.getAdaugareMembruPage().getprenumeT().getText().isEmpty() ||
				libraryView.getAdaugareMembruPage().getemailT().getText().isEmpty() ||
				libraryView.getAdaugareMembruPage().getnrTelefonT().getText().isEmpty() ||
				libraryView.getAdaugareMembruPage().getstradaT().getText().isEmpty() ||
				libraryView.getAdaugareMembruPage().getblocT().getText().isEmpty() ||
				libraryView.getAdaugareMembruPage().getnumarT().getText().isEmpty() ||
				libraryView.getAdaugareMembruPage().getapartamentT().getText().isEmpty() ||
				libraryView.getAdaugareMembruPage().getparolaT().getText().isEmpty() ){
				
				JOptionPane.showMessageDialog(null, "Fill all the informations" , "",JOptionPane.PLAIN_MESSAGE);
				}
			else{
				Adresa adresa = new Adresa(libraryView.getAdaugareMembruPage().getstradaT().getText(),
						Integer.parseInt(libraryView.getAdaugareMembruPage().getnumarT().getText()),
						libraryView.getAdaugareMembruPage().getblocT().getText(),
						Integer.parseInt(libraryView.getAdaugareMembruPage().getapartamentT().getText()));
				Membru membru = new Membru(libraryView.getAdaugareMembruPage().getNumeT().getText(),
						libraryView.getAdaugareMembruPage().getprenumeT().getText(),
						libraryView.getAdaugareMembruPage().getemailT().getText(),
						libraryView.getAdaugareMembruPage().getnrTelefonT().getText(),
						adresa,libraryView.getAdaugareMembruPage().getparolaT().getText()
						);
				membruBLL.insert(membru);
				JOptionPane.showMessageDialog(null, "Success!!!" , "",JOptionPane.PLAIN_MESSAGE);
				}
			
		}
		
	}
	
	class AdaugareCarteListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if (libraryView.getAdaugareCartePage().getIsbnT().getText().isEmpty() ||
					libraryView.getAdaugareCartePage().getTitluT().getText().isEmpty() ||
					libraryView.getAdaugareCartePage().getEdituraT().getText().isEmpty() ||
					libraryView.getAdaugareCartePage().getAnAparitieT().getText().isEmpty() ||
					libraryView.getAdaugareCartePage().getNrPaginiT().getText().isEmpty() ||
					libraryView.getAdaugareCartePage().getNrCopiiDisponibileT().getText().isEmpty() ||
					libraryView.getAdaugareCartePage().getLimbaT().getText().isEmpty() ||
					libraryView.getAdaugareCartePage().getCodT().getText().isEmpty() ||
					libraryView.getAdaugareCartePage().getGenT().getText().isEmpty() ||
					libraryView.getAdaugareCartePage().getAutorT().getText().isEmpty() ){
				JOptionPane.showMessageDialog(null, "Fill all the informations" , "",JOptionPane.PLAIN_MESSAGE);
			}
			else{
				List<String> genuri = new ArrayList<>();
				List<String> autori = new ArrayList<>();
				genuri.add(libraryView.getAdaugareCartePage().getGenT().getText());
				autori.add(libraryView.getAdaugareCartePage().getAutorT().getText());
				Carte carte = new Carte(libraryView.getAdaugareCartePage().getIsbnT().getText(),
						libraryView.getAdaugareCartePage().getTitluT().getText(),
						libraryView.getAdaugareCartePage().getEdituraT().getText(),
						Integer.parseInt(libraryView.getAdaugareCartePage().getAnAparitieT().getText()),
						Integer.parseInt(libraryView.getAdaugareCartePage().getNrPaginiT().getText()),
						Integer.parseInt(libraryView.getAdaugareCartePage().getNrCopiiDisponibileT().getText()),
						libraryView.getAdaugareCartePage().getLimbaT().getText(),
						Float.parseFloat(libraryView.getAdaugareCartePage().getCodT().getText()),
						genuri,autori);
				carteBLL.insert(carte);
				JOptionPane.showMessageDialog(null, "Success!!!" , "",JOptionPane.PLAIN_MESSAGE);
			}
		}
		
	}
			
	
}
