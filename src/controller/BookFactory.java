package controller;

import java.util.HashMap;
import java.util.Map;

import model.Carte;

public class BookFactory {
	private Map<String,Carte> bookMap = new HashMap<>();
	
	public void insertBook(Carte carte){
		Carte book = bookMap.get(carte.getTitlu());
		if (book == null){
			bookMap.put(carte.getTitlu(), carte);
		}
	}
	public Map<String, Carte> getBookMap() {
		return bookMap;
	}
	
	
}
