package model;

import java.util.List;

public class Carte {
	private String ISBN;
	private String titlu;
	private String editura;
	private int anAparitie;
	private int nrPagini;
	private int nrCopiiDisponibile;
	private String limba;
	private float cod;
	private List<String> gen;
	private List<String> autori;
	public Carte(String iSBN, String titlu, String editura, int anAparitie, int nrPagini, int nrCopiiDisponibile,
			String limba, float cod, List<String> gen, List<String> autori) {
		super();
		ISBN = iSBN;
		this.titlu = titlu;
		this.editura = editura;
		this.anAparitie = anAparitie;
		this.nrPagini = nrPagini;
		this.nrCopiiDisponibile = nrCopiiDisponibile;
		this.limba = limba;
		this.cod = cod;
		this.gen = gen;
		this.autori = autori;
	}
	public int getNrCopiiDisponibile() {
		return nrCopiiDisponibile;
	}
	public void setNrCopiiDisponibile(int nrCopiiDisponibile) {
		this.nrCopiiDisponibile = nrCopiiDisponibile;
	}
	public String getISBN() {
		return ISBN;
	}
	public String getTitlu() {
		return titlu;
	}
	public String getEditura() {
		return editura;
	}
	public int getAnAparitie() {
		return anAparitie;
	}
	public int getNrPagini() {
		return nrPagini;
	}
	public String getLimba() {
		return limba;
	}
	public float getCod() {
		return cod;
	}
	public List<String> getGen() {
		return gen;
	}
	public List<String> getAutori() {
		return autori;
	}
	
	
}
