package model;

public class Adresa {
	private String strada;
	private int numar;
	private String bloc;
	private int apartament;
	public Adresa(String strada, int numar, String bloc, int apartament) {
		super();
		this.strada = strada;
		this.numar = numar;
		this.bloc = bloc;
		this.apartament = apartament;
	}
	
	public Adresa(String strada, int numar) {
		super();
		this.strada = strada;
		this.numar = numar;
	}
	public String getStrada() {
		return strada;
	}
	public void setStrada(String strada) {
		this.strada = strada;
	}
	public int getNumar() {
		return numar;
	}
	public void setNumar(int numar) {
		this.numar = numar;
	}
	public String getBloc() {
		return bloc;
	}
	public void setBloc(String bloc) {
		this.bloc = bloc;
	}
	public int getApartament() {
		return apartament;
	}
	public void setApartament(int apartament) {
		this.apartament = apartament;
	}
	
	
}
