package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.swing.JOptionPane;
import javax.activation.*;

public class Membru implements MyObserver{
	private int id;
	private String nume;
	private String prenume;
	private String email;
	private String nrTelefon;
	private Adresa adresa;
	private String parola;
	private Boolean dreptImprumut = true;
	private int nrZileIntarziate = 0;
	private List<ImprumutRezervare> rezervari;
	private List<ImprumutRezervare> imprumuturi;
	private List<ImprumutRezervare> istoric;
	
	public Membru(String nume,String prenume,String email,String nrTelefon,Adresa adresa,String parola){
		this.nume = nume;
		this.prenume = prenume;
		this.email = email;
		this.nrTelefon = nrTelefon;
		this.adresa = adresa;
		this.parola = parola;
		this.rezervari = new ArrayList<>();
		this.imprumuturi = new ArrayList<>();
		this.istoric = new ArrayList<>();
	}
	public Membru(int id,String nume,String prenume,String email,String nrTelefon,Adresa adresa,String parola){
		this.id = id;
		this.nume = nume;
		this.prenume = prenume;
		this.email = email;
		this.nrTelefon = nrTelefon;
		this.adresa = adresa;
		this.parola = parola;
		this.rezervari = new ArrayList<>();
		this.imprumuturi = new ArrayList<>();
		this.istoric = new ArrayList<>();
	}
	public Membru(int id,String nume,String prenume,String email,String nrTelefon,Adresa adresa,String parola,
			List<ImprumutRezervare> rezervari, List<ImprumutRezervare> imprumuturi, List<ImprumutRezervare> istoric, boolean obs){
		this.id = id;
		this.nume = nume;
		this.prenume = prenume;
		this.email = email;
		this.nrTelefon = nrTelefon;
		this.adresa = adresa;
		this.parola = parola;
		this.rezervari = rezervari;
		this.imprumuturi = imprumuturi;
		this.istoric = istoric;
		for(ImprumutRezervare imprumut : imprumuturi){
			imprumut.addObserver(this);
		}
		if (obs){
			this.update();
		}
	}
	
	public Membru(String email, String parola) {
		super();
		this.email = email;
		this.parola = parola;
	}
	
	public int getId() {
		return id;
	}
	public String getParola() {
		return parola;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	
	public String getPrenume() {
		return prenume;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNrTelefon() {
		return nrTelefon;
	}
	public void setNrTelefon(String nrTelefon) {
		this.nrTelefon = nrTelefon;
	}
	public Adresa getAdresa() {
		return adresa;
	}
	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}
	public Boolean getDreptImprumut() {
		return dreptImprumut;
	}
	public void setDreptImprumut(Boolean dreptImprumut) {
		this.dreptImprumut = dreptImprumut;
	}
	public List<ImprumutRezervare> getRezervari() {
		return rezervari;
	}
	public List<ImprumutRezervare> getImprumuturi() {
		return imprumuturi;
	}
	public List<ImprumutRezervare> getIstoric() {
		return istoric;
	}
	public int getNrZileIntarziate() {
		return nrZileIntarziate;
	}
	public void setNrZileIntarziate(int nrZileIntarziate) {
		this.nrZileIntarziate = nrZileIntarziate;
	}
	@Override
	public void update() {
		for(ImprumutRezervare imprumut : this.imprumuturi){
			if (imprumut.hasChanged()){
				Properties props = new Properties();
				props.put("mail.smtp.host", "smtp.gmail.com");
				props.put("mail.smtp.socketFactory.port", "465");
				props.put("mail.smtp.socketFactory.class",
						"javax.net.ssl.SSLSocketFactory");
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.port", "465");

				Session session = Session.getDefaultInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication("grecmihai43@gmail.com","altair1ezioauditore234");
						}
					});

				try {

					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress("grecmihai43@gmail.com"));
					message.setRecipients(Message.RecipientType.TO,
							InternetAddress.parse(email));
					message.setSubject("Returnare carte");
					message.setText("Buna ziua, " +
							"\n\n Va informam ca valabilitatea cartii \""+ imprumut.getCarte()+"\" va expira in 3 zile. O zi frumoasa!");

					Transport.send(message);
				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}
			}
		}
		
	}
	
	
	
	
}
