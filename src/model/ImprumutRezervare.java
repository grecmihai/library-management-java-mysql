package model;

import java.util.Date;
import java.util.Observable;

public class ImprumutRezervare extends MyObservable{
	private String carte;
	private Date dataImprumutului;
	private Date dataValabilitate;
	public ImprumutRezervare(String carte, Date dataImprumutului, Date dataValabilitate) {
		super();
		this.carte = carte;
		this.dataImprumutului = dataImprumutului;
		this.dataValabilitate = dataValabilitate;
		
	}
	public Date getDataValabilitate() {
		return dataValabilitate;
	}
	public void setDataValabilitate(Date dataValabilitate) {
		this.dataValabilitate = dataValabilitate;
	}
	public String getCarte() {
		return carte;
	}
	public Date getDataImprumutului() {
		return dataImprumutului;
	}
	@Override
	public boolean hasChanged() {
		Date date = new Date();
		if (date.getYear() == dataValabilitate.getYear()){
			if(date.getMonth() == dataValabilitate.getMonth()){
				if (date.getDay() == dataValabilitate.getDay() - 3){
					return true;
				}
			}
		}
		return false;
	}
}
