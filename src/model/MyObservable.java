package model;

import java.util.ArrayList;
import java.util.List;

public abstract class MyObservable {
	List<MyObserver> observers;
	public MyObservable(){
		observers = new ArrayList<>();
	}
	public void addObserver(MyObserver observer){
		observers.add(observer);
	}
	public abstract boolean hasChanged();
	
}
