package views;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class CartiImprumutatePage extends JFrame{
	private JPanel panel;
	private JTable table;
	private String[] columns = {"Nume" , "Prenume" , "Titlu" , "Data valabilitate"};
	private Object[][] data;
	private JScrollPane scrollPane;
	
	public CartiImprumutatePage(){
		super("Imprumuturi");
		panel = new JPanel();
		this.setContentPane(panel);
		this.setSize(500,500);
		this.setLocation(800, 300);
		
		panel.setLayout(new BorderLayout());
		panel.setBackground(Color.WHITE);
		
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public String[] getColumns() {
		return columns;
	}

	public void setColumns(String[] columns) {
		this.columns = columns;
	}

	public Object[][] getData() {
		return data;
	}

	public void setData(Object[][] data) {
		this.data = data;
	}
	
	public Object getDataByPosition(int i , int j){
		return data[i][j];
	}
	public void setDataByPosition(String string , int i , int j){
		this.data[i][j] = string;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
	
}
