package views;

public class LibraryView {
	private AdaugarePenalizarePage adaugarePenalizarePage;
	private AdminPage adminPage;
	private CatalogPage catalogPage;
	private CreareUserPage creareUserPage;
	private ImprumutCartePage imprumutCartePage;
	private Log log;
	private Main main;
	private MemberPage memberPage;
	private PlatireAmendaPage platireAmendaPage;
	private PrelungireCartePage prelungireCartePage;
	private ReturnareCartePage returnareCartePage;
	private RezervaPage rezervaPage;
	private CartiImprumutatePage cartiImprumutatePage;
	private AdaugareMembruPage adaugareMembruPage;
	private AdaugareCartePage adaugareCartePage;
	private EditareMembruPage editareMembruPage;
	private MainPage mainPage;
	private MainPageFactory mainPageFactory;
	
	public LibraryView(){
		adaugarePenalizarePage = new AdaugarePenalizarePage();
		adminPage = new AdminPage();
		catalogPage = new CatalogPage();
		creareUserPage = new CreareUserPage();
		imprumutCartePage = new ImprumutCartePage();
		log = new Log();
		memberPage = new MemberPage();
		main = new Main();
		platireAmendaPage = new PlatireAmendaPage();
		prelungireCartePage = new PrelungireCartePage();
		returnareCartePage = new ReturnareCartePage();
		rezervaPage = new RezervaPage();
		cartiImprumutatePage = new CartiImprumutatePage();
		adaugareMembruPage = new AdaugareMembruPage();
		adaugareCartePage = new AdaugareCartePage();
		editareMembruPage = new EditareMembruPage();
		mainPageFactory = new MainPageFactory();
	}

	public AdaugarePenalizarePage getAdaugarePenalizarePage() {
		return adaugarePenalizarePage;
	}

	public AdminPage getAdminPage() {
		return adminPage;
	}

	public CatalogPage getCatalogPage() {
		return catalogPage;
	}

	public CreareUserPage getCreareUserPage() {
		return creareUserPage;
	}

	public ImprumutCartePage getImprumutCartePage() {
		return imprumutCartePage;
	}

	public Log getLog() {
		return log;
	}

	public Main getMain() {
		return main;
	}

	public MemberPage getMemberPage() {
		return memberPage;
	}

	public PlatireAmendaPage getPlatireAmendaPage() {
		return platireAmendaPage;
	}

	public PrelungireCartePage getPrelungireCartePage() {
		return prelungireCartePage;
	}

	public ReturnareCartePage getReturnareCartePage() {
		return returnareCartePage;
	}
	
	public RezervaPage getRezervaPage(){
		return rezervaPage;
	}

	public CartiImprumutatePage getCartiImprumutatePage() {
		return cartiImprumutatePage;
	}

	public AdaugareMembruPage getAdaugareMembruPage() {
		return adaugareMembruPage;
	}

	public AdaugareCartePage getAdaugareCartePage() {
		return adaugareCartePage;
	}

	public EditareMembruPage getEditareMembruPage() {
		return editareMembruPage;
	}

	public MainPage getMainPage() {
		return mainPage;
	}

	public void setMainPage(MainPage mainPage) {
		this.mainPage = mainPage;
	}

	public MainPageFactory getMainPageFactory() {
		return mainPageFactory;
	}
	
	
	
	
	
	
}
