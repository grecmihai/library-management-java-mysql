package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class PlatireAmendaPage extends JFrame {


	private JButton bAdauga;
	private JPanel panel;
	private JTextField nume;
	private JTextField prenume;
	private JLabel mesaj;
	private JLabel numeL,prenumeL;

	public PlatireAmendaPage(){
		super("Platire");
		setSize(400,250);
		setLocation(800,300);
		
		bAdauga = new JButton("Platire");
		panel = new JPanel();
		nume = new JTextField(15);
		prenume = new JTextField(15);
		mesaj = new JLabel("Succes!!!");
		mesaj.setForeground(Color.green);
		mesaj.setVisible(false);
		numeL = new JLabel("Nume:");
		prenumeL = new JLabel("Prenume:");
		panel.setLayout (null); 

		numeL.setFont(new Font(numeL.getFont().getName(),Font.BOLD,numeL.getFont().getSize() + 10));
		prenumeL.setFont(numeL.getFont());
		
		numeL.setBounds(70,30,150,30);
		prenumeL.setBounds(70,75,150,30);
		nume.setBounds(170,30,150,30);
		prenume.setBounds(170,75,150,30);
		bAdauga.setBounds(130,120,80,30);
		mesaj.setBounds(100 , 155 , 150 , 20);

		panel.add(numeL);
		panel.add(prenumeL);
		panel.add(bAdauga);
		panel.add(nume);
		panel.add(prenume);
		panel.add(mesaj);

		getContentPane().add(panel);
	}
	
	public void addPlatireAmendaPageListener(ActionListener listener){
		bAdauga.addActionListener(listener);
	}

	public JButton getbAdauga() {
		return bAdauga;
	}

	public void setbAdauga(JButton bAdauga) {
		this.bAdauga = bAdauga;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTextField getNume() {
		return nume;
	}

	public void setNume(JTextField nume) {
		this.nume = nume;
	}

	public JTextField getPrenume() {
		return prenume;
	}

	public void setPrenume(JTextField prenume) {
		this.prenume = prenume;
	}

	public JLabel getMesaj() {
		return mesaj;
	}

	public void setMesaj(JLabel mesaj) {
		this.mesaj = mesaj;
	}
	
	
	

	
	}