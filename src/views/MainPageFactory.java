package views;

public class MainPageFactory {
	public MainPage getMainPage(String reportPageType, AdminPage adminPage, MemberPage memberPage){
		if (reportPageType == null){
			return null;
		}
		if (reportPageType.equalsIgnoreCase("admin") ){
			return adminPage;
		}
		if (reportPageType.equalsIgnoreCase("member") ){
			return memberPage;
		}
		return null;
	}
}
