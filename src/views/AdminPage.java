package views;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class AdminPage extends JFrame implements MainPage{
	private JPanel adminPanel;
	private JButton addMember ,updateMember,addBook, adPen , prelCar , imprCar , retCar , plAme , actualRez , cartImp , cat;
	
	public AdminPage(){
		
		super("Biblioteca judeteana Salaj");
		adminPanel = new JPanel();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(adminPanel);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		adminPanel.setLayout(new GridLayout(4,3));
		addMember = new JButton("Adaugare membru");
		updateMember = new JButton("Actualizare membru");
		addBook = new JButton("Adaugare carte");
		adPen = new JButton("Adaugare penalizare");
		prelCar = new JButton("Prelungire carte");
		imprCar = new JButton("Imprumut carte");
		retCar = new JButton("Returnare carte");
		plAme = new JButton("Platire amenda");
		actualRez = new JButton("Actualizare rezervari");
		cartImp = new JButton("Carti imprumuntate momentan");
		cat = new JButton("Catalog");
		JButton ll = new JButton("");
		
		adminPanel.add(addMember);
		adminPanel.add(updateMember);
		adminPanel.add(addBook);
		adminPanel.add(adPen);
		adminPanel.add(prelCar);
		adminPanel.add(imprCar);
		adminPanel.add(retCar);
		adminPanel.add(plAme);
		adminPanel.add(actualRez);
		adminPanel.add(cartImp);
		adminPanel.add(cat);
		adminPanel.add(ll);
		
		
		addMember.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , addMember.getFont().getSize() + 20));
		updateMember.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , updateMember.getFont().getSize() + 20));
		addBook.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , addBook.getFont().getSize() + 20));
		adPen.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , adPen.getFont().getSize() + 20));
		prelCar.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , prelCar.getFont().getSize() + 20));
		imprCar.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , imprCar.getFont().getSize() + 20));
		retCar.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , retCar.getFont().getSize() + 20));
		plAme.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , plAme.getFont().getSize() + 20));
		actualRez.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , actualRez.getFont().getSize() + 20));
		cartImp.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , cartImp.getFont().getSize() + 20));
		cat.setFont(new Font(addMember.getFont().getName() , Font.BOLD  , cat.getFont().getSize() + 20));
		
		Color color = new Color(244 , 189 , 0);
		addMember.setBackground(color);
		updateMember.setBackground(color);
		addBook.setBackground(color);
		adPen.setBackground(color);
		prelCar.setBackground(color);
		imprCar.setBackground(color);
		retCar.setBackground(color);
		plAme.setBackground(color);
		actualRez.setBackground(color);
		cartImp.setBackground(color);
		cat.setBackground(color);
		ll.setBackground(color);
		
	}
	
	public void addAddMemberListener(ActionListener listener){
		addMember.addActionListener(listener);
	}
	public void addUpdateMemberListener(ActionListener listener){
		updateMember.addActionListener(listener);
	}
	public void addAddBookListener(ActionListener listener){
		addBook.addActionListener(listener);
	}
	public void addAdPenListener(ActionListener listener){
		adPen.addActionListener(listener);
	}
	public void addPrCarListener(ActionListener listener){
		prelCar.addActionListener(listener);
	}
	public void addImCarListener(ActionListener listener){
		imprCar.addActionListener(listener);
	}
	public void addReCarListener(ActionListener listener){
		retCar.addActionListener(listener);
	}
	public void addPlAmeListener(ActionListener listener){
		plAme.addActionListener(listener);
	}
	public void AddActualRezListener(ActionListener listener){
		actualRez.addActionListener(listener);
	}
	public void addCaImpListener(ActionListener listener){
		cartImp.addActionListener(listener);
	}
	public void addCatListener(ActionListener listener){
		cat.addActionListener(listener);
	}

	public JPanel getAdminPanel() {
		return adminPanel;
	}

	public void setAdminPanel(JPanel adminPanel) {
		this.adminPanel = adminPanel;
	}


	public JButton getAddMember() {
		return addMember;
	}

	public void setAddMember(JButton addMember) {
		this.addMember = addMember;
	}

	public JButton getUpdateMember() {
		return updateMember;
	}

	public void setUpdateMember(JButton updateMember) {
		this.updateMember = updateMember;
	}

	public JButton getAddBook() {
		return addBook;
	}

	public void setAddBook(JButton addBook) {
		this.addBook = addBook;
	}

	public JButton getAdPen() {
		return adPen;
	}

	public void setAdPen(JButton adPen) {
		this.adPen = adPen;
	}

	public JButton getPrelCar() {
		return prelCar;
	}

	public void setPrelCar(JButton prelCar) {
		this.prelCar = prelCar;
	}

	public JButton getImprCar() {
		return imprCar;
	}

	public void setImprCar(JButton imprCar) {
		this.imprCar = imprCar;
	}

	public JButton getRetCar() {
		return retCar;
	}

	public void setRetCar(JButton retCar) {
		this.retCar = retCar;
	}

	public JButton getPlAme() {
		return plAme;
	}

	public void setPlAme(JButton plAme) {
		this.plAme = plAme;
	}

	public JButton getActualRez() {
		return actualRez;
	}

	public void setActualRez(JButton actualRez) {
		this.actualRez = actualRez;
	}

	public JButton getCartImp() {
		return cartImp;
	}

	public void setCartImp(JButton cartImp) {
		this.cartImp = cartImp;
	}

	public JButton getCat() {
		return cat;
	}

	public void setCat(JButton cat) {
		this.cat = cat;
	}

	@Override
	public void setVisibility(boolean visible) {
		this.setVisible(visible);
	}
	
	
}
