package views;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class EditareMembruPage extends JFrame{
	private JLabel numeL,emailL,telefonL,stradaL,numarL,blocL,apartamentL;
	private JTextField numeT,emailT,telefonT,stradaT,numarT,blocT,apartamentT;
	private JPanel panel;
	private JButton executeButton;
	private JComboBox comboBox;
	
	public EditareMembruPage(){
		super("EDIT CUSTOMER");
		
		this.setLocation(500, 200);
		this.setSize(700, 750);
		panel = new JPanel();
		this.setContentPane(panel);
		panel.setLayout(null);
		
		numeL = new JLabel("Nume:");
		emailL = new JLabel("e-mail:");
		telefonL = new JLabel("Telefon:");
		stradaL = new JLabel("Strada:");
		numarL = new JLabel("Numar:");
		blocL = new JLabel("Bloc:");
		apartamentL = new JLabel("Apartament:");
		numeT = new JTextField(30);
		emailT = new JTextField(30);
		telefonT = new JTextField(30);
		stradaT = new JTextField(30);
		numarT = new JTextField(30);
		blocT = new JTextField(30);
		apartamentT = new JTextField(30);
		executeButton = new JButton("EDITEAZA");
		
		numeL.setFont(new Font(numeL.getFont().getName(),Font.BOLD,numeL.getFont().getSize() + 5));
		emailL.setFont(numeL.getFont());
		telefonL.setFont(numeL.getFont());
		stradaL.setFont(numeL.getFont());
		numarL.setFont(numeL.getFont());
		blocL.setFont(numeL.getFont());
		apartamentL.setFont(numeL.getFont());
		numeT.setFont(new Font(numeT.getFont().getName(),Font.PLAIN,numeT.getFont().getSize() + 5));
		emailT.setFont(numeT.getFont());
		telefonT.setFont(numeT.getFont());
		stradaT.setFont(numeT.getFont());
		numarT.setFont(numeT.getFont());
		blocT.setFont(numeT.getFont());
		apartamentT.setFont(numeT.getFont());
		
		numeL.setBounds(150,80,150,20);
		emailL.setBounds(150,150,150,20);
		telefonL.setBounds(150,220,150,20);
		stradaL.setBounds(150,290,150,20);
		numarL.setBounds(150,360,150,20);
		blocL.setBounds(150,430,150,20);
		apartamentL.setBounds(150,500,150,20);
		
		numeT.setBounds(250,80,300,40);
		emailT.setBounds(250,150,300,40);
		telefonT.setBounds(250,220,300,40);
		stradaT.setBounds(250,290,300,40);
		numarT.setBounds(250,360,300,40);
		blocT.setBounds(250,430,300,40);
		apartamentT.setBounds(250,500,300,40);
		
		executeButton.setBounds(250,600,150,40);

		
		panel.add(numeL);
		panel.add(emailL);
		panel.add(telefonL);
		panel.add(stradaL);
		panel.add(numarL);
		panel.add(blocL);
		panel.add(apartamentL);
		panel.add(numeT);
		panel.add(emailT);
		panel.add(telefonT);
		panel.add(stradaT);
		panel.add(numarT);
		panel.add(blocT);
		panel.add(apartamentT);
		panel.add(executeButton);
		
	}

	
	public JTextField getNumeT() {
		return numeT;
	}


	public void setNumeT(JTextField numeT) {
		this.numeT = numeT;
	}


	public JTextField getEmailT() {
		return emailT;
	}


	public void setEmailT(JTextField emailT) {
		this.emailT = emailT;
	}


	public JTextField getTelefonT() {
		return telefonT;
	}


	public void setTelefonT(JTextField telefonT) {
		this.telefonT = telefonT;
	}


	public JTextField getStradaT() {
		return stradaT;
	}


	public void setStradaT(JTextField stradaT) {
		this.stradaT = stradaT;
	}


	public JTextField getNumarT() {
		return numarT;
	}


	public void setNumarT(JTextField numarT) {
		this.numarT = numarT;
	}


	public JTextField getBlocT() {
		return blocT;
	}


	public void setBlocT(JTextField blocT) {
		this.blocT = blocT;
	}


	public JTextField getApartamentT() {
		return apartamentT;
	}


	public void setApartamentT(JTextField apartamentT) {
		this.apartamentT = apartamentT;
	}


	public void addExecuteButtonListener(ActionListener listener){
		executeButton.addActionListener(listener);
	}
	public void setComboBox(JComboBox comboBox){
		if (this.comboBox != null)
			panel.remove(this.comboBox);
		this.comboBox = comboBox;
		this.comboBox.setBounds(250, 15, 250, 40);
		panel.add(this.comboBox);
		panel.revalidate();
	}
	public void setTexts(String nume,String email,String telefon,String strada,String numar,String bloc,String apartament){
		numeT.setText(nume);
		emailT.setText(email);
		telefonT.setText(telefon);
		stradaT.setText(strada);
		numarT.setText(numar);
		blocT.setText(bloc);
		apartamentT.setText(apartament);
	}

	public JComboBox getComboBox() {
		return comboBox;
	}
	public void addComboBoxListener(ActionListener listener, JComboBox box){
		box.addActionListener(listener);
	}
}
