package views;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AdaugareMembruPage extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	private JLabel numeL,prenumeL,emailL,nrTelefonL,stradaL,blocL,numarL,apartamentL,parolaL;
	private JTextField numeT,prenumeT,emailT,nrTelefonT,stradaT,blocT,numarT,apartamentT,parolaT;
	private JPanel panel;
	private JButton executeButton;
	
	public AdaugareMembruPage(){
		super("Add member");
		this.setLocation(300, 200);
		this.setSize(1200, 700);
		panel = new JPanel();
		this.setContentPane(panel);
		panel.setLayout(null);
		
		numeL = new JLabel("Nume:");
		prenumeL = new JLabel("Prenume:");
		emailL = new JLabel("Email:");
		nrTelefonL = new JLabel("Telefon:");
		stradaL = new JLabel("Strada:");

		blocL = new JLabel("Bloc:");
		numarL = new JLabel("Numar:");
		apartamentL = new JLabel("Apartament:");
		parolaL = new JLabel("Parola:");
		
		numeT = new JTextField(30);
		prenumeT = new JTextField(30);
		emailT = new JTextField(30);
		nrTelefonT = new JTextField(30);
		stradaT = new JTextField(30);
		
		blocT = new JTextField(30);
		numarT = new JTextField(30);
		apartamentT = new JTextField(30);
		parolaT = new JTextField(30);
		
		
		executeButton = new JButton("ADAUGA");
		
		numeL.setFont(new Font(numeL.getFont().getName(),Font.BOLD,numeL.getFont().getSize() + 5));
		prenumeL.setFont(numeL.getFont());
		emailL.setFont(numeL.getFont());
		nrTelefonL.setFont(numeL.getFont());
		stradaL.setFont(numeL.getFont());
		
		blocL.setFont(numeL.getFont());
		numarL.setFont(numeL.getFont());
		apartamentL.setFont(numeL.getFont());
		parolaL.setFont(numeL.getFont());
		
		
		numeT.setFont(new Font(numeT.getFont().getName(),Font.PLAIN,numeT.getFont().getSize() + 5));
		prenumeT.setFont(numeT.getFont());
		emailT.setFont(numeT.getFont());
		nrTelefonT.setFont(numeT.getFont());
		stradaT.setFont(numeT.getFont());
		
		blocT.setFont(numeT.getFont());
		numarT.setFont(numeT.getFont());
		apartamentT.setFont(numeT.getFont());
		parolaT.setFont(numeT.getFont());
		
		
		numeL.setBounds(150,80,150,20);
		prenumeL.setBounds(150,150,150,20);
		emailL.setBounds(150,220,150,20);
		nrTelefonL.setBounds(150,290,150,20);
		stradaL.setBounds(150,360,150,20);
		
		numarL.setBounds(650,80,150,20);
		blocL.setBounds(650,150,150,20);
		apartamentL.setBounds(650,220,150,20);
		parolaL.setBounds(650,290,150,20);
		
		
		numeT.setBounds(250,80,300,40);
		prenumeT.setBounds(250,150,300,40);
		emailT.setBounds(250,220,300,40);
		nrTelefonT.setBounds(250,290,300,40);
		stradaT.setBounds(250,360,300,40);
		
		numarT.setBounds(750,80,300,40);
		blocT.setBounds(750,150,300,40);
		apartamentT.setBounds(750,220,300,40);
		parolaT.setBounds(750,290,300,40);
		
		
		executeButton.setBounds(550,450,150,40);
		
		panel.add(numeL);
		panel.add(prenumeL);
		panel.add(emailL);
		panel.add(nrTelefonL);
		panel.add(stradaL);
		
		panel.add(numarL);
		panel.add(blocL);
		panel.add(apartamentL);
		panel.add(parolaL);
		
		panel.add(numeT);
		panel.add(prenumeT);
		panel.add(emailT);
		panel.add(nrTelefonT);
		panel.add(stradaT);
		
		panel.add(numarT);
		panel.add(blocT);
		panel.add(apartamentT);
		panel.add(parolaT);
		
		panel.add(executeButton);
		
	}
	public void addExecuteButtonListener(ActionListener listener){
		executeButton.addActionListener(listener);
	}
	
	public JTextField getNumeT() {
		return numeT;
	}
	public void setNumeT(JTextField numeT) {
		this.numeT = numeT;
	}
	public JTextField getprenumeT() {
		return prenumeT;
	}
	public void setprenumeT(JTextField prenumeT) {
		this.prenumeT = prenumeT;
	}
	public JTextField getemailT() {
		return emailT;
	}
	public void setemailT(JTextField emailT) {
		this.emailT = emailT;
	}
	public JTextField getnrTelefonT() {
		return nrTelefonT;
	}
	public void setnrTelefonT(JTextField nrTelefonT) {
		this.nrTelefonT = nrTelefonT;
	}
	public JTextField getstradaT() {
		return stradaT;
	}
	public void setstradaT(JTextField stradaT) {
		this.stradaT = stradaT;
	}
	
	public JTextField getblocT() {
		return blocT;
	}
	public void setblocT(JTextField blocT) {
		this.blocT = blocT;
	}
	public JTextField getnumarT() {
		return numarT;
	}
	public void setnumarT(JTextField numarT) {
		this.numarT = numarT;
	}
	public JTextField getapartamentT() {
		return apartamentT;
	}
	public void setapartamentT(JTextField apartamentT) {
		this.apartamentT = apartamentT;
	}
	public JTextField getparolaT() {
		return parolaT;
	}
	public void setparolaT(JTextField parolaT) {
		this.parolaT = parolaT;
	}
	
	public JPanel getPanel() {
		return panel;
	}
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
	public JButton getExecuteButton() {
		return executeButton;
	}
	public void setExecuteButton(JButton executeButton) {
		this.executeButton = executeButton;
	}
	
	
}