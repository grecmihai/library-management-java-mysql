package views;


import java.awt.*;

import javax.swing.*;

public class JPanelWithBackground{
	private ImageIcon backgroundImage;
	JLabel label;
	
	public JPanelWithBackground(String fileName){
		backgroundImage = new ImageIcon(getClass().getResource(fileName)); 
		label = new JLabel();
		label.setLayout(null);
		label.setIcon(backgroundImage);
	}

	public JLabel getLabel() {
		return label;
	}
	
}
