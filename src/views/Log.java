package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class Log extends JFrame {


	private JButton blogin;
	private JPanel panel ;
	private JTextField txuser ;
	private JPasswordField pass;
	private JLabel mesaj;
	private JLabel contL,passL;
	public Log(){
		super("");
		setSize(400,270);
		setLocation(800,380);

		
		blogin = new JButton("Login");
		panel = new JPanel();
		txuser = new JTextField("" , 15);
		pass  = new JPasswordField("" , 15);
		mesaj = new JLabel("Cont sau parola gresita");
		contL = new JLabel("Cont:");
		passL = new JLabel("Pass:");
		mesaj.setForeground(Color.RED);
		mesaj.setVisible(false);
		panel.setLayout (null); 
		contL.setFont(new Font(contL.getFont().getName(),Font.BOLD,contL.getFont().getSize() + 10));
		passL.setFont(contL.getFont());
		
		contL.setBounds(70,30,150,30);
		passL.setBounds(70,85,150,30);
		txuser.setBounds(150,30,150,30);
		pass.setBounds(150,85,150,30);
		blogin.setBounds(140,130,80,30);
		mesaj.setBounds(70,170,150,20);
		
		panel.add(contL);
		panel.add(passL);
		panel.add(blogin);
		panel.add(txuser);
		panel.add(pass);
		panel.add(mesaj);
		txuser.requestFocusInWindow();
		

		getContentPane().add(panel);
	}
	
	public void addLoginListener(ActionListener listener){
		blogin.addActionListener(listener);
	}

	public JButton getBlogin() {
		return blogin;
	}

	public void setBlogin(JButton blogin) {
		this.blogin = blogin;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTextField getTxuser() {
		return txuser;
	}

	public void setTxuser(JTextField txuser) {
		this.txuser = txuser;
	}

	public JPasswordField getPass() {
		return pass;
	}

	public void setPass(JPasswordField pass) {
		this.pass = pass;
	}

	public JLabel getMesaj() {
		return mesaj;
	}

	public void setMesaj(JLabel mesaj) {
		this.mesaj = mesaj;
	}
	
	
	

	
	}
