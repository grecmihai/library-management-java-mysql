package views;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class CatalogPage extends JFrame{
	private JPanel panel;
	private JPanel search;
	private JTextField searchtxt;
	private JButton searchbtn;
	private String[] columns = {"Titlu" , "Nume" , "Prenume" , "Gen" , "Limba" ,"Cod", "Carti la raft"};
	private Object[][] data;
	private JPanel tablePanel;
	private JScrollPane scrollPane;
	private JTable table;
	public CatalogPage(){
		super("Catalog");
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		
		Color color = new Color(153 , 217 , 234);
		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBackground(color);
		JLabel text = new JLabel("                                                     BIBLIOTECA JUDETEANA SALAJ");
		text.setFont(new Font(text.getFont().getName() , Font.BOLD + Font.ITALIC , text.getFont().getSize() + 30));
		text.setForeground(Color.BLACK);
		text.setBackground(Color.white);
		panel.add(text , BorderLayout.NORTH);
		this.setContentPane(panel);
		
		search = new JPanel();
		search.setLayout(new BorderLayout());
		search.setBackground(Color.white);
		panel.add(search , BorderLayout.CENTER);
		
		JPanel empty1 , empty2 , empty3;
		empty1 = new JPanel();
		empty2 = new JPanel();
		empty3 = new JPanel();
		empty1.setBackground(color);
		empty2.setBackground(color);
		empty3.setBackground(color);
		panel.add(empty3 , BorderLayout.SOUTH);
		panel.add(empty2, BorderLayout.WEST);
		panel.add(empty1, BorderLayout.EAST);
		
		JPanel searchBar = new JPanel();
		searchtxt = new JTextField(50);
		searchbtn = new JButton("CAUTA");
		searchBar.setLayout(new FlowLayout());
		searchBar.add(searchtxt);
		searchBar.add(searchbtn);
		search.add(searchBar , BorderLayout.NORTH);
		
		tablePanel = new JPanel();
		tablePanel.setLayout(new BorderLayout());
		table = new JTable();
		tablePanel.add(table.getTableHeader(), BorderLayout.NORTH);
		tablePanel.add(table , BorderLayout.CENTER);
		search.add(tablePanel , BorderLayout.CENTER);
		
		
		
		
	}
	
	public void addSearchbtnListener(ActionListener listener){
		searchbtn.addActionListener(listener);
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JPanel getSearch() {
		return search;
	}

	public void setSearch(JPanel search) {
		this.search = search;
	}

	public JTextField getSearchtxt() {
		return searchtxt;
	}

	public void setSearchtxt(JTextField searchtxt) {
		this.searchtxt = searchtxt;
	}

	public JButton getSearchbtn() {
		return searchbtn;
	}

	public void setSearchbtn(JButton searchbtn) {
		this.searchbtn = searchbtn;
	}

	public String[] getColumns() {
		return columns;
	}

	public void setColumns(String[] columns) {
		this.columns = columns;
	}

	public Object[][] getData() {
		return data;
	}

	public void setData(Object[][] data) {
		this.data = data;
	}

	public JPanel getTablePanel() {
		return tablePanel;
	}

	public void setTablePanel(JPanel tablePanel) {
		this.tablePanel = tablePanel;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
	public Object getDataByPosition(int i , int j){
		return data[i][j];
	}
	public void setDataByPosition(String string , int i , int j){
		this.data[i][j] = string;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}
	
	
}
