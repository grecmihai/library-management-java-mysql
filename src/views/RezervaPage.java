package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class RezervaPage extends JFrame {


	private JButton bRezerva;
	private JPanel panel;
	private JTextField titlu;
	private final JLabel mesaj;
	private JLabel titluL;

	public RezervaPage(){
		super("Rezervare");
		setSize(350,200);
		setLocation(500,280);
		bRezerva = new JButton("Rezerva");
		panel = new JPanel();
		titlu = new JTextField(15);
		titluL = new JLabel("Titlu:");
		mesaj = new JLabel("Cartea este imprumutata");
		
		panel.setLayout (null); 
		titluL.setFont(new Font(titluL.getFont().getName(),Font.BOLD,titluL.getFont().getSize() + 10));
		
		titluL.setBounds(70,30,150,30);
		titlu.setBounds(150,30,150,30);
		mesaj.setBounds(70,115,150,20);
		bRezerva.setBounds(110,75,80,30);

		panel.add(titluL);
		panel.add(bRezerva);
		panel.add(titlu);
		panel.add(mesaj);
		getContentPane().add(panel);
		
		mesaj.setForeground(Color.RED);
		mesaj.setVisible(false);
		
	}
	
	public void addRezervaListener(ActionListener listener){
		bRezerva.addActionListener(listener);
	}

	public JButton getbRezerva() {
		return bRezerva;
	}

	public void setbRezerva(JButton bRezerva) {
		this.bRezerva = bRezerva;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTextField getTitlu() {
		return titlu;
	}

	public void setTitlu(JTextField titlu) {
		this.titlu = titlu;
	}

	public JLabel getMesaj() {
		return mesaj;
	}
	

	
	
	}