package views;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Main extends JFrame{
	
		
		private JPanelWithBackground panel = new JPanelWithBackground("26102.jpg");
		private JPanel acces , center;
		private JLabel text , cont , password , error;
		private JTextField contText;
		private JPasswordField passwordText;
		private JButton login;
		
		public Main(){
			super("Biblioteca judeteana Salaj");
			
			acces = new JPanel();

			Color color = new Color(163 , 163 , 163);
			Color color2 = new Color(179 , 0 , 0);
			acces.setBackground(color2);
			panel.getLabel().add(acces);
			acces.setLocation(500 , 100);
			acces.setSize(1000, 800);
			acces.setLayout(new BorderLayout());
			
			text = new JLabel("              BINE ATI VENIT!");
			text.setFont(new Font(text.getFont().getName() , Font.BOLD + Font.ITALIC , text.getFont().getSize() + 50));
			text.setForeground(Color.black);
			acces.add(text , BorderLayout.NORTH);
			
			
			login = new JButton("LOG IN");
			login.setBackground(color2);
			login.setForeground(Color.cyan);
			login.setBorderPainted(false);
			login.setFont(new Font(login.getFont().getName() , Font.BOLD , login.getFont().getSize() + 30));
			JPanel butoane = new JPanel();
			butoane.setLayout(new FlowLayout());
			butoane.add(login);
			butoane.setBackground(color2);
			butoane.setPreferredSize(new Dimension(200 , 200));
			login.setPreferredSize(new Dimension(250 , 125));
			login.setBorderPainted(false);
			

			
			JPanel citate = new JPanel();
			citate.setBackground(color2);
			citate.setLayout(new BoxLayout(citate , BoxLayout.Y_AXIS));
			JLabel citat1 = new JLabel("  'Lectura este o forma a fericirii.");
			citat1.setFont(new Font(text.getFont().getName() , Font.BOLD  , text.getFont().getSize() - 10));
			JLabel citat2 = new JLabel("  Ultima la care vom renunta.'");
			citat2.setFont(new Font(text.getFont().getName() , Font.BOLD  , text.getFont().getSize() - 10));
			JLabel citat3 = new JLabel("                                  (Fernando Savater)");
			citat3.setFont(new Font(text.getFont().getName() , Font.BOLD  , text.getFont().getSize() - 10));
			JLabel citat4 = new JLabel("  'Daca nu-ti place sa citesti , inseamna ");
			citat4.setFont(new Font(text.getFont().getName() , Font.BOLD  , text.getFont().getSize() - 10));
			JLabel citat5 = new JLabel("  ca n-ai gasit cartea potrivita'");
			citat5.setFont(new Font(text.getFont().getName() , Font.BOLD  , text.getFont().getSize() - 10));
			JLabel citat6 = new JLabel("                                            (J.K.Rowling)");
			citat6.setFont(new Font(text.getFont().getName() , Font.BOLD  , text.getFont().getSize() - 10));
			citat1.setForeground(color.WHITE);
			citat2.setForeground(Color.WHITE);
			citat3.setForeground(Color.WHITE);
			citat4.setForeground(Color.WHITE);
			citat5.setForeground(Color.WHITE);
			citat6.setForeground(Color.WHITE);
			
			

			citate.add(Box.createRigidArea(new Dimension(0,100)));
			citate.add(citat1);
			citate.add(citat2);
			citate.add(citat3);
			citate.add(citat4);
			citate.add(citat5);
			citate.add(citat6);
			
			
			acces.add(citate , BorderLayout.CENTER);
			acces.add(butoane, BorderLayout.SOUTH);
			
			
			
			this.setContentPane(panel.getLabel());
			this.setLayout(new BorderLayout());
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setExtendedState(JFrame.MAXIMIZED_BOTH);
			
			
		}
		
		public void addLoginButtonListener(ActionListener listener){
			login.addActionListener(listener);
		}

		public JPanelWithBackground getPanel() {
			return panel;
		}

		public void setPanel(JPanelWithBackground panel) {
			this.panel = panel;
		}

		public JPanel getAcces() {
			return acces;
		}

		public void setAcces(JPanel acces) {
			this.acces = acces;
		}

		public JPanel getCenter() {
			return center;
		}

		public void setCenter(JPanel center) {
			this.center = center;
		}

		public JLabel getText() {
			return text;
		}

		public void setText(JLabel text) {
			this.text = text;
		}

		public JLabel getCont() {
			return cont;
		}

		public void setCont(JLabel cont) {
			this.cont = cont;
		}

		public JLabel getPassword() {
			return password;
		}

		public void setPassword(JLabel password) {
			this.password = password;
		}

		public JLabel getError() {
			return error;
		}

		public void setError(JLabel error) {
			this.error = error;
		}

		public JTextField getContText() {
			return contText;
		}

		public void setContText(JTextField contText) {
			this.contText = contText;
		}

		public JPasswordField getPasswordText() {
			return passwordText;
		}

		public void setPasswordText(JPasswordField passwordText) {
			this.passwordText = passwordText;
		}

		public JButton getLogin() {
			return login;
		}

		public void setLogin(JButton login) {
			this.login = login;
		}
		
		
	
}
