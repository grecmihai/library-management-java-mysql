package views;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AdaugareCartePage extends JFrame{
	private JLabel isbnL,titluL,edituraL,anAparitieL,nrPaginiL,limbaL,nrCopiiDisponibileL,codL,genL,autorL;
	private JTextField isbnT,titluT,edituraT,anAparitieT,nrPaginiT,limbaT,nrCopiiDisponibileT,codT,genT,autorT;
	private JPanel panel;
	private JButton executeButton;
	
	public AdaugareCartePage() {
		super("Add member");
		this.setLocation(300, 200);
		this.setSize(1200, 700);
		panel = new JPanel();
		this.setContentPane(panel);
		panel.setLayout(null);
		
		isbnL = new JLabel("ISBN:");
		titluL = new JLabel("Titlu:");
		edituraL = new JLabel("Editura:");
		anAparitieL = new JLabel("An Aparitie:");
		nrPaginiL = new JLabel("Nr Pagini:");

		limbaL = new JLabel("Limba:");
		nrCopiiDisponibileL = new JLabel("Nr Copii:");
		codL = new JLabel("Cod:");
		genL = new JLabel("Gen:");
		autorL = new JLabel("Autor:");
		
		isbnT = new JTextField(30);
		titluT = new JTextField(30);
		edituraT = new JTextField(30);
		anAparitieT = new JTextField(30);
		nrPaginiT = new JTextField(30);
		
		limbaT = new JTextField(30);
		nrCopiiDisponibileT = new JTextField(30);
		codT = new JTextField(30);
		genT = new JTextField(30);
		autorT = new JTextField(30);
		
		
		executeButton = new JButton("ADAUGA");
		
		isbnL.setFont(new Font(isbnL.getFont().getName(),Font.BOLD,isbnL.getFont().getSize() + 5));
		titluL.setFont(isbnL.getFont());
		edituraL.setFont(isbnL.getFont());
		anAparitieL.setFont(isbnL.getFont());
		nrPaginiL.setFont(isbnL.getFont());
		
		limbaL.setFont(isbnL.getFont());
		nrCopiiDisponibileL.setFont(isbnL.getFont());
		codL.setFont(isbnL.getFont());
		genL.setFont(isbnL.getFont());
		autorL.setFont(isbnL.getFont());
		
		isbnT.setFont(new Font(isbnT.getFont().getName(),Font.PLAIN,isbnT.getFont().getSize() + 5));
		titluT.setFont(isbnT.getFont());
		edituraT.setFont(isbnT.getFont());
		anAparitieT.setFont(isbnT.getFont());
		nrPaginiT.setFont(isbnT.getFont());
		
		limbaT.setFont(isbnT.getFont());
		nrCopiiDisponibileT.setFont(isbnT.getFont());
		codT.setFont(isbnT.getFont());
		genT.setFont(isbnT.getFont());
		autorT.setFont(isbnL.getFont());
		
		isbnL.setBounds(150,80,150,20);
		titluL.setBounds(150,150,150,20);
		edituraL.setBounds(150,220,150,20);
		anAparitieL.setBounds(150,290,150,20);
		nrPaginiL.setBounds(150,360,150,20);
		
		nrCopiiDisponibileL.setBounds(650,80,150,20);
		limbaL.setBounds(650,150,150,20);
		codL.setBounds(650,220,150,20);
		genL.setBounds(650,290,150,20);
		autorL.setBounds(650,360,150,20);
		
		
		isbnT.setBounds(250,80,300,40);
		titluT.setBounds(250,150,300,40);
		edituraT.setBounds(250,220,300,40);
		anAparitieT.setBounds(250,290,300,40);
		nrPaginiT.setBounds(250,360,300,40);
		
		nrCopiiDisponibileT.setBounds(750,80,300,40);
		limbaT.setBounds(750,150,300,40);
		codT.setBounds(750,220,300,40);
		genT.setBounds(750,290,300,40);
		autorT.setBounds(750,360,300,40);
		
		
		executeButton.setBounds(550,450,150,40);
		
		panel.add(isbnL);
		panel.add(titluL);
		panel.add(edituraL);
		panel.add(anAparitieL);
		panel.add(nrPaginiL);
		
		panel.add(nrCopiiDisponibileL);
		panel.add(limbaL);
		panel.add(codL);
		panel.add(genL);
		panel.add(autorL);
		
		panel.add(isbnT);
		panel.add(titluT);
		panel.add(edituraT);
		panel.add(anAparitieT);
		panel.add(nrPaginiT);
		
		panel.add(nrCopiiDisponibileT);
		panel.add(limbaT);
		panel.add(codT);
		panel.add(genT);
		panel.add(autorT);
		
		panel.add(executeButton);
		
	}
	public void addExecuteButtonListener(ActionListener listener){
		executeButton.addActionListener(listener);
	}
	public JLabel getIsbnL() {
		return isbnL;
	}
	public void setIsbnL(JLabel isbnL) {
		this.isbnL = isbnL;
	}
	public JLabel getTitluL() {
		return titluL;
	}
	public void setTitluL(JLabel titluL) {
		this.titluL = titluL;
	}
	public JLabel getEdituraL() {
		return edituraL;
	}
	public void setEdituraL(JLabel edituraL) {
		this.edituraL = edituraL;
	}
	public JLabel getAnAparitieL() {
		return anAparitieL;
	}
	public void setAnAparitieL(JLabel anAparitieL) {
		this.anAparitieL = anAparitieL;
	}
	public JLabel getNrPaginiL() {
		return nrPaginiL;
	}
	public void setNrPaginiL(JLabel nrPaginiL) {
		this.nrPaginiL = nrPaginiL;
	}
	public JLabel getLimbaL() {
		return limbaL;
	}
	public void setLimbaL(JLabel limbaL) {
		this.limbaL = limbaL;
	}
	public JLabel getNrCopiiDisponibileL() {
		return nrCopiiDisponibileL;
	}
	public void setNrCopiiDisponibileL(JLabel nrCopiiDisponibileL) {
		this.nrCopiiDisponibileL = nrCopiiDisponibileL;
	}
	public JLabel getCodL() {
		return codL;
	}
	public void setCodL(JLabel codL) {
		this.codL = codL;
	}
	public JLabel getGenL() {
		return genL;
	}
	public void setGenL(JLabel genL) {
		this.genL = genL;
	}
	public JLabel getAutorL() {
		return autorL;
	}
	public void setAutorL(JLabel autorL) {
		this.autorL = autorL;
	}
	public JTextField getIsbnT() {
		return isbnT;
	}
	public void setIsbnT(JTextField isbnT) {
		this.isbnT = isbnT;
	}
	public JTextField getTitluT() {
		return titluT;
	}
	public void setTitluT(JTextField titluT) {
		this.titluT = titluT;
	}
	public JTextField getEdituraT() {
		return edituraT;
	}
	public void setEdituraT(JTextField edituraT) {
		this.edituraT = edituraT;
	}
	public JTextField getAnAparitieT() {
		return anAparitieT;
	}
	public void setAnAparitieT(JTextField anAparitieT) {
		this.anAparitieT = anAparitieT;
	}
	public JTextField getNrPaginiT() {
		return nrPaginiT;
	}
	public void setNrPaginiT(JTextField nrPaginiT) {
		this.nrPaginiT = nrPaginiT;
	}
	public JTextField getLimbaT() {
		return limbaT;
	}
	public void setLimbaT(JTextField limbaT) {
		this.limbaT = limbaT;
	}
	public JTextField getNrCopiiDisponibileT() {
		return nrCopiiDisponibileT;
	}
	public void setNrCopiiDisponibileT(JTextField nrCopiiDisponibileT) {
		this.nrCopiiDisponibileT = nrCopiiDisponibileT;
	}
	public JTextField getCodT() {
		return codT;
	}
	public void setCodT(JTextField codT) {
		this.codT = codT;
	}
	public JTextField getGenT() {
		return genT;
	}
	public void setGenT(JTextField genT) {
		this.genT = genT;
	}
	public JTextField getAutorT() {
		return autorT;
	}
	public void setAutorT(JTextField autorT) {
		this.autorT = autorT;
	}
	public JPanel getPanel() {
		return panel;
	}
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
	public JButton getExecuteButton() {
		return executeButton;
	}
	public void setExecuteButton(JButton executeButton) {
		this.executeButton = executeButton;
	}
	
	
}
