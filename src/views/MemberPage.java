package views;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
public class MemberPage extends JFrame implements MainPage{
	
	private JPanelWithBackground panel = new JPanelWithBackground("book.jpg");
	private JPanel helper;
	private JPanel afisaj;
	private JButton imprumuturi ;
	private JButton istoric ;
	private JButton rezervari ;
	private JButton catalog ;
	private JButton rezerva;
	
	public MemberPage(){
		
		super("Biblioteca judeteana Salaj");
		Color color = new Color(255 , 255 , 134);
		this.setContentPane(panel.getLabel());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		helper = new JPanel();
		helper.setLayout(new GridLayout(9,1));
		
		panel.getLabel().add(helper);
		helper.setLocation(100 , 250);
		helper.setSize(250 , 500);
		helper.setBackground(color);
		
		imprumuturi = new JButton("Imprumuturi");
		istoric = new JButton("Istoric");
		rezervari = new JButton("Rezervari");
		rezerva = new JButton("Rezerva");
		catalog = new JButton("Catalog");
		JButton empty1 = new JButton("");
		JButton empty2 = new JButton("");
		
		helper.add(empty2);
		helper.add(empty1);
		helper.add(imprumuturi);
		helper.add(istoric);
		helper.add(rezervari);
		helper.add(rezerva);
		helper.add(catalog);
		
		imprumuturi.setBackground(color);
		istoric.setBackground(color);
		rezervari.setBackground(color);
		rezerva.setBackground(color);
		catalog.setBackground(color);
		empty1.setBackground(color);
		empty2.setBackground(color);
		empty1.setBorderPainted(false);
		empty2.setBorderPainted(false);
		imprumuturi.setBorderPainted(false);
		istoric.setBorderPainted(false);
		rezervari.setBorderPainted(false);
		rezerva.setBorderPainted(false);
		catalog.setBorderPainted(false);
		imprumuturi.setFont(new Font(imprumuturi.getFont().getName() , Font.BOLD  , imprumuturi.getFont().getSize() + 10));
		istoric.setFont(new Font(imprumuturi.getFont().getName() , Font.BOLD  , istoric.getFont().getSize() + 10));
		rezervari.setFont(new Font(imprumuturi.getFont().getName() , Font.BOLD  , rezervari.getFont().getSize() + 10));
		rezerva.setFont(new Font(imprumuturi.getFont().getName() , Font.BOLD  , catalog.getFont().getSize() + 10));
		catalog.setFont(new Font(imprumuturi.getFont().getName() , Font.BOLD  , catalog.getFont().getSize() + 10));
		
		
		afisaj = new JPanel();
		panel.getLabel().add(afisaj);
		afisaj.setLocation(500 , 100);
		afisaj.setSize(1000 , 800);
		afisaj.setBackground(color);
		afisaj.setLayout(new BorderLayout());
		
		
	}
	
	public void addImprumuturiListener(ActionListener listener){
		imprumuturi.addActionListener(listener);
	}
	public void addIstoricListener(ActionListener listener){
		istoric.addActionListener(listener);
	}
	public void addRezervariListener(ActionListener listener){
		rezervari.addActionListener(listener);
	}
	public void addRezervaListener(ActionListener listener){
		rezerva.addActionListener(listener);
	}
	public void addCatalogListener(ActionListener listener){
		catalog.addActionListener(listener);
	}

	public JPanelWithBackground getPanel() {
		return panel;
	}

	public void setPanel(JPanelWithBackground panel) {
		this.panel = panel;
	}

	public JPanel getHelper() {
		return helper;
	}

	public void setHelper(JPanel helper) {
		this.helper = helper;
	}

	public JPanel getAfisaj() {
		return afisaj;
	}

	public void setAfisaj(JPanel afisaj) {
		this.afisaj = afisaj;
	}

	public JButton getImprumuturi() {
		return imprumuturi;
	}

	public void setImprumuturi(JButton imprumuturi) {
		this.imprumuturi = imprumuturi;
	}

	public JButton getIstoric() {
		return istoric;
	}

	public void setIstoric(JButton istoric) {
		this.istoric = istoric;
	}

	public JButton getRezervari() {
		return rezervari;
	}

	public void setRezervari(JButton rezervari) {
		this.rezervari = rezervari;
	}

	public JButton getCatalog() {
		return catalog;
	}

	public void setCatalog(JButton catalog) {
		this.catalog = catalog;
	}

	public JButton getRezerva() {
		return rezerva;
	}

	public void setRezerva(JButton rezerva) {
		this.rezerva = rezerva;
	}

	@Override
	public void setVisibility(boolean visible) {
		this.setVisible(visible);
	}
	
	
	
	
}
