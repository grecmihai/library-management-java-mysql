package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class PrelungireCartePage extends JFrame {


	private JButton bprelungire;
	private JPanel panel;
	private JTextField nume;
	private JTextField prenume;
	private JTextField titlu;
	private JLabel mesaj;
	private JLabel numeL,prenumeL,titluL;

	public PrelungireCartePage(){
		super("Prelungire");
		setSize(400,300);
		setLocation(800,300);
		
		bprelungire = new JButton("Prelungire");
		panel = new JPanel();
		nume = new JTextField(15);
		prenume = new JTextField(15);
		titlu = new JTextField(15);
		mesaj = new JLabel("Succes!!!");
		mesaj.setForeground(Color.green);
		mesaj.setVisible(false);
		numeL = new JLabel("Nume:");
		prenumeL = new JLabel("Prenume:");
		titluL = new JLabel("Titlu:");
		
		panel.setLayout (null); 
		
		numeL.setFont(new Font(numeL.getFont().getName(),Font.BOLD,numeL.getFont().getSize() + 10));
		prenumeL.setFont(numeL.getFont());
		titluL.setFont(numeL.getFont());
		
		
		numeL.setBounds(50,30,150,30);
		prenumeL.setBounds(50,75,150,30);
		nume.setBounds(170,30,150,30);
		prenume.setBounds(170,75,150,30);
		titlu.setBounds(170,120,150,30);
		titluL.setBounds(50,120,150,30);
		bprelungire.setBounds(120,165,110,30);
		mesaj.setBounds(100 , 200 , 150 , 20);

		panel.add(numeL);
		panel.add(prenumeL);
		panel.add(titluL);
		panel.add(bprelungire);
		panel.add(nume);
		panel.add(prenume);
		panel.add(titlu);
		panel.add(mesaj);

		getContentPane().add(panel);
	}
	
	public void addPrelungireCartePageListener(ActionListener listener){
		bprelungire.addActionListener(listener);
	}

	public JButton getBprelungire() {
		return bprelungire;
	}

	public void setBprelungire(JButton bprelungire) {
		this.bprelungire = bprelungire;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTextField getNume() {
		return nume;
	}

	public void setNume(JTextField nume) {
		this.nume = nume;
	}

	public JTextField getPrenume() {
		return prenume;
	}

	public void setPrenume(JTextField prenume) {
		this.prenume = prenume;
	}

	public JTextField getTitlu() {
		return titlu;
	}

	public void setTitlu(JTextField titlu) {
		this.titlu = titlu;
	}

	public JLabel getMesaj() {
		return mesaj;
	}

	public void setMesaj(JLabel mesaj) {
		this.mesaj = mesaj;
	}
	
	

	
	
	}