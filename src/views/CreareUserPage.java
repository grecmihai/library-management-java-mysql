package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class CreareUserPage extends JFrame {


	private JButton bcreate;
	private JPanel panel;
	private JTextField txuser;
	private JTextField pass;
	private JLabel mesaj;

	public CreareUserPage(){
		super("");
		setSize(310,230);
		setLocation(800,300);
		
		bcreate = new JButton("Creare");
		panel = new JPanel();
		txuser = new JTextField("Cont" , 15);
		pass = new JTextField("Parola" , 15);
		mesaj = new JLabel("Succes!!!");
		mesaj.setForeground(Color.green);
		mesaj.setVisible(false);
		
		
		panel.setLayout (null); 


		txuser.setBounds(70,30,150,20);
		pass.setBounds(70,65,150,20);
		bcreate.setBounds(110,100,80,20);
		mesaj.setBounds(120 , 125 , 80 , 20);

		panel.add(bcreate);
		panel.add(txuser);
		panel.add(pass);
		panel.add(mesaj);

		getContentPane().add(panel);
	}
	
	public void addCreareUserPageListener(ActionListener listener){
		bcreate.addActionListener(listener);
	}

	public JButton getBcreate() {
		return bcreate;
	}

	public void setBcreate(JButton bcreate) {
		this.bcreate = bcreate;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTextField getTxuser() {
		return txuser;
	}

	public void setTxuser(JTextField txuser) {
		this.txuser = txuser;
	}

	public JTextField getPass() {
		return pass;
	}

	public void setPass(JTextField pass) {
		this.pass = pass;
	}


	public JLabel getMesaj() {
		return mesaj;
	}

	public void setMesaj(JLabel mesaj) {
		this.mesaj = mesaj;
	}
	
	
	

	
	}