package dao;

import java.util.List;

import model.Carte;

public interface CarteDAO {
	public List<String> getAuthors(String titlu);
	public List<String> getGenres(String titlu);
	public Carte findByISBN(String ISBN);
	public Carte findByTitle(String titlu);
	public List<Carte> findBooksByAuthor(String autor);
	public List<Carte> findBooksByGenre(String gen);
	public List<Carte> findBooksByTitle(String titlu);
	public void insert(Carte carte);
	

}
