package dao;

import java.util.List;
import model.ImprumutRezervare;
import model.Membru;

public interface MembruDAO {
	public Membru getMemberByName(String nume, String prenume, boolean obs);
	public Membru getMemberByEmail(String email);
	public String getPass(String email);
	public List<ImprumutRezervare> getImprumuturiCurente(String email);
	public List<ImprumutRezervare> getRezervari(String email);
	public List<ImprumutRezervare> getIstoric(String email);
	//<0 daca nu gaseste membrul,0 in caz de succes
	public int adaugarePenalizare(String nume, String prenume);
	//<0 in caz ca nu gaseste membrul sau cartea, 0 in caz de succes
	public int prelungireCarte(String nume, String prenume,String titlu);
	//-1 daca membrul sau cartea sunt prost date
	//-2 daca nu mai exista copie disponibila
	//-3 daca membrul nu are drept de imprumut
	//-4 daca membru are deja imprumutate 3 carti
	//0 la succes
	public int imprumutCarte(String nume, String prenume,String titlu);
	//-1 daca informatiile sunt prost date
	//-2 daca informatiile sunt bune, dar membrul nu are cartea
	//0 la succes
	public int returnareCarte(String nume, String prenume,String titlu);
	//-1 daca numele nu e bun
	//-2 daca membrul nu are de fapt amenda de platit
	//0 in caz de succes
	public int platireAmenda(String nume, String prenume);
	public void actualizareRezervari();
	public List<Membru> getAllMembers(boolean obs);
	//-1 daca membrul sau cartea sunt prost date
	//-2 daca nu mai exista copie disponibila
	//-3 daca membrul nu are drept de rezervare
	//-4 daca membru are deja rezervate 3 carti
	// 0 la succes
	public int rezervareCarte(String nume, String prenume,String titlu);
	public int insert(Membru membru);
	public boolean update(Membru membru , String cust);
}
