package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Carte;

public class CarteDAOImpl implements CarteDAO{
	protected static final Logger LOGGER = Logger.getLogger(MembruDAO.class.getName());
	private static Connection dbConnection = ConnectionFactory.getConnection();
	private static final String getAuthorss = "select Nume, Prenume from autor "+
			"join carti_autor on autor.idAutor = carti_autor.idAutor "+
			"join carti on carti_autor.isbn = carti.isbn "+
			"where titlu = ?;";
	private static final String getGenres = "select gencol from gen "+
			"join carti_gen on gen.idGen = carti_gen.idGen "+
			"join carti on carti_gen.isbn = carti.isbn "+
			"where titlu = ?; ";
	private static final String findISBN = "select * from carti where isbn = ?;";
	private static final String findTitlu = "select * from carti where titlu = ?;";
	private static final String booksByAuthor = "select * from carti "+
			"join carti_autor on carti.isbn = carti_autor.isbn "+
			"join autor on carti_autor.idAutor = autor.idAutor "+
			"where nume = ? or prenume = ?; ";
private static final String booksByGenre = "select * from carti "+
			"join carti_gen on carti.isbn = carti_gen.isbn "+
			"join gen on carti_gen.idGen = gen.idGen "+
			"where gencol = ?;";
private static final String booksByTitleAprox = "select * from carti where titlu like ? or titlu like ? or titlu like ?;";
private static final String inserareCarte = "insert into Carti values (?,?,?,?,?,?,?,?)";
private static final String inserareAutor = "insert into Autor(Nume , Prenume) values (?,?);";
private static final String inserareCarteAutor = "insert into Carti_Autor (Isbn , idAutor) values (?,?);";
private static final String existaAutor = "select Nume, Prenume from autor where nume = ? and prenume = ?;";

	
	public List<String> getAuthors(String titlu){
		List<String> toReturn = new ArrayList<>();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try{
			findStatement = dbConnection.prepareStatement(getAuthorss);
			findStatement.setString(1, titlu);
			rs = findStatement.executeQuery();
			while(rs.next()){
				String nume = rs.getString("Nume");
				String prenume = rs.getString("Prenume");
				String autor = nume + " " + prenume;
				toReturn.add(autor);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.WARNING,"CarteDAO:getAuthors" + e.getMessage());
		}
		finally{
		ConnectionFactory.close(rs);
		ConnectionFactory.close(findStatement);
		}
		return toReturn;
	}
	public List<String> getGenres(String titlu){
		List<String> toReturn = new ArrayList<>();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try{
			findStatement = dbConnection.prepareStatement(getGenres);
			findStatement.setString(1, titlu);
			rs = findStatement.executeQuery();
			rs.next();
			String gen = rs.getString("gencol");
			toReturn.add(gen);
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.WARNING,"CarteDAO:findById " + e.getMessage());
		}
		finally{
		ConnectionFactory.close(rs);
		ConnectionFactory.close(findStatement);
		}
		return toReturn;
	}
	public Carte findByISBN(String ISBN){
		Carte toReturn = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try{
			findStatement = dbConnection.prepareStatement(findISBN);
			findStatement.setString(1, ISBN);
			rs = findStatement.executeQuery();
			rs.next();
			String titlu = rs.getString("Titlu");
			String editura = rs.getString("Editura");
			int anAparitie = rs.getInt("AnAparitie");
			int nrPagini = rs.getInt("NrPagini");
			int nrCopii = rs.getInt("NrCopiiDisponibile");
			String limba = rs.getString("Limba");
			float cod = rs.getFloat("Cod");
			List<String> autori = getAuthors(titlu);
			List<String> genuri = getGenres(titlu);
			toReturn = new Carte(ISBN,titlu,editura,anAparitie,nrPagini,nrCopii,limba,cod,genuri,autori);
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.WARNING,"CarteDAO:findByISBN " + e.getMessage());
		}
		finally{
		ConnectionFactory.close(rs);
		ConnectionFactory.close(findStatement);
		}
		return toReturn;
		
	}
	
	public Carte findByTitle(String titlu){
		Carte toReturn = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try{
			findStatement = dbConnection.prepareStatement(findTitlu);
			findStatement.setString(1, titlu);
			rs = findStatement.executeQuery();
			rs.next();
			String ISBN = rs.getString("Isbn");
			String editura = rs.getString("Editura");
			int anAparitie = rs.getInt("AnAparitie");
			int nrPagini = rs.getInt("NrPagini");
			int nrCopii = rs.getInt("NrCopiiDisponibile");
			String limba = rs.getString("Limba");
			float cod = rs.getFloat("Cod");
			List<String> autori = getAuthors(titlu);
			List<String> genuri = getGenres(titlu);
			toReturn = new Carte(ISBN,titlu,editura,anAparitie,nrPagini,nrCopii,limba,cod,genuri,autori);
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.WARNING,"CarteDAO:findByTitle " + e.getMessage());
		}
		finally{
		ConnectionFactory.close(rs);
		ConnectionFactory.close(findStatement);
		}
		return toReturn;
		
	}
	
	public List<Carte> findBooksByAuthor(String autor){
		List<Carte> toReturn = new ArrayList<>();
		PreparedStatement getStatement = null;
		ResultSet rs = null;
		try{
			getStatement = dbConnection.prepareStatement(booksByAuthor);
			getStatement.setString(1, autor.split(" ")[0]);
			getStatement.setString(2, autor.split(" ")[0]);
			rs = getStatement.executeQuery();
			while(rs.next()){
				String isbn = rs.getString("ISBN");
				String titlu = rs.getString("titlu");
				String editura = rs.getString("Editura");
				int anAparitie = rs.getInt("AnAparitie");
				int nrPagini = rs.getInt("NrPagini");
				int nrCopii = rs.getInt("NrCopiiDisponibile");
				String limba = rs.getString("Limba");
				float cod = rs.getFloat("Cod");
				List<String> autori = getAuthors(titlu);
				List<String> genuri = getGenres(titlu);
				toReturn.add(new Carte(isbn,titlu,editura,anAparitie,nrPagini,nrCopii,limba,cod,genuri,autori));
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CarteDAO:findBooksByAuthor" + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	public List<Carte> findBooksByGenre(String gen){
		List<Carte> toReturn = new ArrayList<>();
		PreparedStatement getStatement = null;
		ResultSet rs = null;
		try{
			getStatement = dbConnection.prepareStatement(booksByGenre);
			getStatement.setString(1, gen);
			rs = getStatement.executeQuery();
			while(rs.next()){
				String isbn = rs.getString("ISBN");
				String titlu = rs.getString("titlu");
				String editura = rs.getString("Editura");
				int anAparitie = rs.getInt("AnAparitie");
				int nrPagini = rs.getInt("NrPagini");
				int nrCopii = rs.getInt("NrCopiiDisponibile");
				String limba = rs.getString("Limba");
				float cod = rs.getFloat("Cod");
				List<String> autori = getAuthors(titlu);
				List<String> genuri = getGenres(titlu);
				toReturn.add(new Carte(isbn,titlu,editura,anAparitie,nrPagini,nrCopii,limba,cod,genuri,autori));
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CarteDAO:findBooksByGenre " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	public List<Carte> findBooksByTitle(String titlu){
		List<Carte> toReturn = new ArrayList<>();
		PreparedStatement getStatement = null;
		ResultSet rs = null;
		String titlu1 = "%" + titlu;
		String titlu2 = titlu + "%";
		String titlu3 = "%" + titlu + "%";
		try{
			getStatement = dbConnection.prepareStatement(booksByTitleAprox);
			getStatement.setString(1, titlu1);
			getStatement.setString(2, titlu2);
			getStatement.setString(3, titlu3);
			rs = getStatement.executeQuery();
			while(rs.next()){
				String isbn = rs.getString("ISBN");
				String Titlu = rs.getString("titlu");
				String editura = rs.getString("Editura");
				int anAparitie = rs.getInt("AnAparitie");
				int nrPagini = rs.getInt("NrPagini");
				int nrCopii = rs.getInt("NrCopiiDisponibile");
				String limba = rs.getString("Limba");
				float cod = rs.getFloat("Cod");
				List<String> autori = getAuthors(Titlu);
				List<String> genuri = getGenres(Titlu);
				toReturn.add(new Carte(isbn,Titlu,editura,anAparitie,nrPagini,nrCopii,limba,cod,genuri,autori));
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CarteDAO:findBooksByTitle " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	public void insert(Carte carte){
		PreparedStatement insertStatement= null;
		PreparedStatement insertStatementAutor = null;
		PreparedStatement insertStatementCarteAutor = null;
		PreparedStatement helper = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		try{
			insertStatement = dbConnection.prepareStatement(inserareCarte);
			insertStatement.setString(1, carte.getISBN());
			insertStatement.setString(2, carte.getTitlu());
			insertStatement.setString(3, carte.getEditura());
			insertStatement.setInt(4, carte.getAnAparitie());
			insertStatement.setInt(5, carte.getNrPagini());
			insertStatement.setInt(6, carte.getNrCopiiDisponibile());
			insertStatement.setString(7, carte.getLimba());
			insertStatement.setFloat(8, carte.getCod());
			insertStatement.executeUpdate();
			
			for(String s : carte.getAutori()){
				helper = dbConnection.prepareStatement(existaAutor);
				helper.setString(1, s.split(" ")[0]);
				helper.setString(2, s.split(" ")[1]);
				rs = helper.executeQuery();
				if (!rs.next()){
					insertStatementAutor = dbConnection.prepareStatement(inserareAutor,Statement.RETURN_GENERATED_KEYS);
					insertStatementAutor.setString(1, s.split(" ")[0]);
					insertStatementAutor.setString(2, s.split(" ")[1]);
					insertStatementAutor.executeUpdate();
					
					rs2 = insertStatementAutor.getGeneratedKeys();
					int id;
					if (rs2.next()){
						id = rs2.getInt(1);
						insertStatementCarteAutor = dbConnection.prepareStatement(inserareCarteAutor);
						insertStatementCarteAutor.setString(1, carte.getISBN());
						insertStatementCarteAutor.setInt(2, id);
						insertStatementCarteAutor.executeUpdate();
					}
					
				}
			}
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CarteDAO:insert " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(insertStatementAutor);
			ConnectionFactory.close(insertStatementCarteAutor);
			ConnectionFactory.close(helper);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(rs2);
		}
	}
}
