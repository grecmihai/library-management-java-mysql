package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Adresa;
import model.Carte;
import model.ImprumutRezervare;
import model.Membru;

public class MembruDAOImpl implements MembruDAO{
	protected static final Logger LOGGER = Logger.getLogger(MembruDAO.class.getName());
	private static Connection dbConnection = ConnectionFactory.getConnection();
	private static final String findPass = "select parola from user join membru on cont=email where email = ?;";
	private static final String getBooksByEmail = "select carti.Titlu ,imprumuturicurente.DataImprumutului,  imprumuturicurente.DataValabilitate from imprumuturicurente "+
			"join carti on carti.Isbn = imprumuturicurente.Isbn "+
			"join membru on membru.idMembru = imprumuturicurente.idMembru "+
			"where email = ?;";
	private static final String getBooksByEmail2 = "select carti.Titlu ,rezervariinternet.DataRezervarii,  rezervariinternet.DataValabilitate from rezervariinternet "+
			"join carti on carti.Isbn = rezervariinternet.Isbn "+
			"join membru on membru.idMembru = rezervariinternet.idMembru "+
			"where email = ?;";
	private static final String getBooksByEmail3 = "select carti.Titlu ,istoricimprumuturi.DataImprumutului,  istoricimprumuturi.DataReturnarii, membru.email from istoricimprumuturi "+
			"join carti on carti.Isbn = istoricimprumuturi.Isbn "+
			"join membru on membru.idMembru = istoricimprumuturi.idMembru "+
			"where email = ?;";
	private static final String getMemberByName = "select * from membru "+
			"join validitateImprumut on membru.idMembru = validitateImprumut.idMembru "+
			"join penalizari on membru.idMembru = penalizari.idMembru "+
			"join user on membru.email = user.cont "+
			"join adresa on membru.idMembru = adresa.idMembru "+
			"where membru.Nume = ? and membru.prenume = ?;";
	private static final String getMemberByEmail = "select * from membru "+
			"join validitateImprumut on membru.idMembru = validitateImprumut.idMembru "+
			"join penalizari on membru.idMembru = penalizari.idMembru "+
			"join user on membru.email = user.cont "+
			"join adresa on membru.idMembru = adresa.idMembru "+
			"where membru.email = ?;";
	private static final String penalizare = "call adaugarePenalizare(?,?);";
	private static final String prelungire = "call prelungireCarte(?,?,?);";
	private static final String imprumut = "call imprumutCarte(?,?,?);";
	private static final String retur = "call returnareCarte(?,?,?);";
	private static final String amenda = "call platireAmenda(?,?);";
	private static final String actualizare = "call stergereRezervari();";
	private static final String rezervare = "call rezervareCarte(?,?,?);";
	private static final String getMembers = "Select * from membru;";
	private static final String getMembru = "Select nume , prenume from membru where nume = ? and prenume = ?";
	private static final String getTitlu = "Select titlu from carti where titlu = ?";
	private static final String inserareMembru = "insert into Membru (Nume , Prenume , Email , NrTelefon) values (?,?,?,?);";
	private static final String inserareAdresa = "insert into Adresa values (?,?,?,?,?);";
	private static final String inserareUser = "insert into user (cont , parola) values (?,?);";
	private static final String updateMembru = "update Membru set nume = ? ,prenume = ?, email = ? , nrTelefon = ? where idMembru = ?";
	private static final String updateAdresa = "update Adresa set strada = ? ,numar = ?, bloc = ? , apartament = ? where idMembru = ?";
	private static final String inserarePenalizari = "insert into penalizari values(?,0);";
	private static final String inserareValiditate = "insert into validitateimprumut values (?,1);";
	
	public Membru getMemberByName(String nume, String prenume, boolean obs){
		Membru toReturn = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(getMemberByName);
			findStatement.setString(1, nume);
			findStatement.setString(2, prenume);
			rs = findStatement.executeQuery();
			rs.next();
			int id = rs.getInt("idMembru");
			String email = rs.getString("email");
			String telefon = rs.getString("NrTelefon");
			int drept = rs.getInt("dreptImprumut");
			Boolean dreptImprumut = false;
			if (drept == 1){
				dreptImprumut = true;
			}
			int nrZileIntarziate = rs.getInt("NrZileIntarziate");
			String parola = rs.getString("Parola");
			String strada = rs.getString("Strada");
			int numar = rs.getInt("Numar");
			String bloc = rs.getString("Bloc");
			int apartament = rs.getInt("apartament");
			Adresa adresa = new Adresa(strada,numar,bloc,apartament);
			List<ImprumutRezervare> imprumuturi = getImprumuturiCurente(email);
			List<ImprumutRezervare> rezervari = getRezervari(email);
			List<ImprumutRezervare> istoric = getIstoric(email);
			toReturn = new Membru(id,nume,prenume,email,telefon,adresa,parola,rezervari,imprumuturi,istoric,obs);
			toReturn.setNrZileIntarziate(nrZileIntarziate);
			toReturn.setDreptImprumut(dreptImprumut);
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.WARNING,"MembruDAO:getMemberByName " + e.getMessage());
		}
		finally{
		ConnectionFactory.close(rs);
		ConnectionFactory.close(findStatement);
		}
		return toReturn;
	}
	public Membru getMemberByEmail(String email){
		Membru toReturn = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(getMemberByEmail);
			findStatement.setString(1, email);
			rs = findStatement.executeQuery();
			rs.next();
			int id = rs.getInt("idMembru");
			String nume = rs.getString("Nume");
			String prenume = rs.getString("Prenume");
			String telefon = rs.getString("NrTelefon");
			int drept = rs.getInt("dreptImprumut");
			Boolean dreptImprumut = false;
			if (drept == 1){
				dreptImprumut = true;
			}
			int nrZileIntarziate = rs.getInt("NrZileIntarziate");
			String parola = rs.getString("Parola");
			String strada = rs.getString("Strada");
			int numar = rs.getInt("Numar");
			String bloc = rs.getString("Bloc");
			int apartament = rs.getInt("apartament");
			Adresa adresa = new Adresa(strada,numar,bloc,apartament);
			List<ImprumutRezervare> imprumuturi = getImprumuturiCurente(email);
			List<ImprumutRezervare> rezervari = getRezervari(email);
			List<ImprumutRezervare> istoric = getIstoric(email);
			toReturn = new Membru(id,nume,prenume,email,telefon,adresa,parola,rezervari,imprumuturi,istoric,false);
			toReturn.setNrZileIntarziate(nrZileIntarziate);
			toReturn.setDreptImprumut(dreptImprumut);
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.WARNING,"MembruDAO:getMemberByEmail " + e.getMessage());
		}
		finally{
		ConnectionFactory.close(rs);
		ConnectionFactory.close(findStatement);
		}
		return toReturn;
	}

	
	public String getPass(String email){
		if (email.equals("admin")){
			return "admin";
		}
		String toReturn = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findPass);
			findStatement.setString(1, email);
			rs = findStatement.executeQuery();
			rs.next();
			toReturn = rs.getString("parola");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.WARNING,"MembruDAO:getPass " + e.getMessage());
		}
		finally{
		ConnectionFactory.close(rs);
		ConnectionFactory.close(findStatement);
		}
		return toReturn;
	}
	public List<ImprumutRezervare> getImprumuturiCurente(String email){
		List<ImprumutRezervare> toReturn = new ArrayList<>();
		PreparedStatement getStatement = null;
		ResultSet rs = null;
		try{
			getStatement = dbConnection.prepareStatement(getBooksByEmail);
			getStatement.setString(1, email);
			rs = getStatement.executeQuery();
			while(rs.next()){
				String titlu = rs.getString("Titlu");
				Date dataImprumutului = rs.getDate("DataImprumutului");
				Date dataValabilitate = rs.getDate("DataValabilitate");
				toReturn.add(new ImprumutRezervare(titlu,dataImprumutului,dataValabilitate));
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:getImprumuturiCurente " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	public List<ImprumutRezervare> getRezervari(String email){
		List<ImprumutRezervare> toReturn = new ArrayList<>();
		PreparedStatement getStatement = null;
		ResultSet rs = null;
		try{
			getStatement = dbConnection.prepareStatement(getBooksByEmail2);
			getStatement.setString(1, email);
			rs = getStatement.executeQuery();
			while(rs.next()){
				String titlu = rs.getString("Titlu");
				Date dataImprumutului = rs.getDate("DataRezervarii");
				Date dataValabilitate = rs.getDate("DataValabilitate");
				toReturn.add(new ImprumutRezervare(titlu,dataImprumutului,dataValabilitate));
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:getRezervari" + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	public List<ImprumutRezervare> getIstoric(String email){
		List<ImprumutRezervare> toReturn = new ArrayList<>();
		PreparedStatement getStatement = null;
		ResultSet rs = null;
		try{
			getStatement = dbConnection.prepareStatement(getBooksByEmail3);
			getStatement.setString(1, email);
			rs = getStatement.executeQuery();
			while(rs.next()){
				String titlu = rs.getString("Titlu");
				Date dataImprumutului = rs.getDate("DataImprumutului");
				Date dataValabilitate = rs.getDate("DataReturnarii");
				toReturn.add(new ImprumutRezervare(titlu,dataImprumutului,dataValabilitate));
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:getIstoric " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	//<0 daca nu gaseste membrul,0 in caz de succes
	public int adaugarePenalizare(String nume, String prenume){
		int toReturn = -1;
		CallableStatement statement = null;
		PreparedStatement helper = null;
		ResultSet rs = null;
		try{
			helper = dbConnection.prepareStatement(getMembru);
			helper.setString(1, nume);
			helper.setString(2, prenume);
			rs = helper.executeQuery();
			if(rs.next()){
				toReturn = 0;
				statement = dbConnection.prepareCall(penalizare);
				statement.setString(1, nume);
				statement.setString(2, prenume);
				statement.execute();
			}
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:adaugarePenalizare " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(statement);
			ConnectionFactory.close(helper);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	//<0 in caz ca nu gaseste membrul sau cartea, 0 in caz de succes
	public int prelungireCarte(String nume, String prenume,String titlu){
		int toReturn = -1;
		CallableStatement statement = null;
		PreparedStatement helper = null;
		PreparedStatement helper2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		try{
			helper = dbConnection.prepareStatement(getMembru);
			helper.setString(1, nume);
			helper.setString(2, prenume);
			rs = helper.executeQuery();
			helper2 = dbConnection.prepareStatement(getTitlu);
			helper2.setString(1, titlu);
			rs2 = helper2.executeQuery();
			if(rs.next() && rs2.next()){
				toReturn = 0;
				statement = dbConnection.prepareCall(prelungire);
				statement.setString(1, nume);
				statement.setString(2, prenume);
				statement.setString(3, titlu);
				statement.execute();
			}
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:prelungireCarte " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(statement);
			ConnectionFactory.close(helper);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	//-1 daca membrul sau cartea sunt prost date
	//-2 daca nu mai exista copie disponibila
	//-3 daca membrul nu are drept de imprumut
	//-4 daca membru are deja imprumutate 3 carti
	//0 la succes
	public int imprumutCarte(String nume, String prenume,String titlu){
		int toReturn = -1;
		CallableStatement statement = null;
		PreparedStatement helper = null;
		PreparedStatement helper2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		try{
			helper = dbConnection.prepareStatement(getMembru);
			helper.setString(1, nume);
			helper.setString(2, prenume);
			rs = helper.executeQuery();
			helper2 = dbConnection.prepareStatement(getTitlu);
			helper2.setString(1, titlu);
			rs2 = helper2.executeQuery();
			if(rs.next() && rs2.next()){
				CarteDAO carteDAO = new CarteDAOImpl();
				Carte carte = carteDAO.findByTitle(titlu);
				if(carte.getNrCopiiDisponibile() == 0){
					toReturn = -2;
				}
				else{
					Membru membru = getMemberByName(nume,prenume,false);
					if(membru.getDreptImprumut() == false){
						toReturn = -3;
					}
					else{
						if (membru.getImprumuturi().size() == 3){
							toReturn = -4;
						}
						else{
							toReturn = 0;
							statement = dbConnection.prepareCall(imprumut);
							statement.setString(1, nume);
							statement.setString(2, prenume);
							statement.setString(3, titlu);
							statement.execute();
						}
					}
				}
			}
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:imprumutCarte " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(statement);
			ConnectionFactory.close(helper);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	//-1 daca informatiile sunt prost date
	//-2 daca informatiile sunt bune, dar membrul nu are cartea
	//0 la succes
	public int returnareCarte(String nume, String prenume,String titlu){
		int toReturn = -1;
		CallableStatement statement = null;
		PreparedStatement helper = null;
		PreparedStatement helper2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		try{
			helper = dbConnection.prepareStatement(getMembru);
			helper.setString(1, nume);
			helper.setString(2, prenume);
			rs = helper.executeQuery();
			helper2 = dbConnection.prepareStatement(getTitlu);
			helper2.setString(1, titlu);
			rs2 = helper2.executeQuery();
			if(rs.next() && rs2.next()){
				Membru membru = getMemberByName(nume,prenume,false);
				int exist = 0;
				for (ImprumutRezervare e : membru.getImprumuturi()){
					if (e.getCarte().equals(titlu))
						exist = 1;
				}
				if (exist == 0){
					toReturn = -2;
				}
				else{
					statement = dbConnection.prepareCall(retur);
					statement.setString(1, nume);
					statement.setString(2, prenume);
					statement.setString(3, titlu);
					statement.execute();
				}
			}
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:returnareCarte " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(statement);
			ConnectionFactory.close(helper);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	//-1 daca numele nu e bun
	//-2 daca membrul nu are de fapt amenda de platit
	//0 in caz de succes
	public int platireAmenda(String nume, String prenume){
		int toReturn = -1;
		CallableStatement statement = null;
		PreparedStatement helper = null;
		ResultSet rs = null;
		try{
			helper = dbConnection.prepareStatement(getMembru);
			helper.setString(1, nume);
			helper.setString(2, prenume);
			rs = helper.executeQuery();
			if(rs.next()){
				Membru membru = getMemberByName(nume,prenume,false);
				if (membru.getDreptImprumut() == true){
					toReturn = -2;
				}
				else{
					toReturn = 0;
					statement = dbConnection.prepareCall(amenda);
					statement.setString(1, nume);
					statement.setString(2, prenume);
					statement.execute();
				}
			}
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:platireAmenda " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(statement);
			ConnectionFactory.close(helper);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	public void actualizareRezervari(){
		CallableStatement statement = null;
		PreparedStatement helper = null;
		ResultSet rs = null;
		try{
				statement = dbConnection.prepareCall(actualizare);
				statement.execute();
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:actualizareRezervari " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(statement);
			ConnectionFactory.close(helper);
			ConnectionFactory.close(rs);
		}
	}
	
	public List<Membru> getAllMembers(boolean obs){
		List<Membru> toReturn = new ArrayList<>();
		Statement getStatement = null;
		ResultSet rs = null;
		try{
			getStatement = dbConnection.createStatement();
			rs = getStatement.executeQuery(getMembers);
			while(rs.next()){
				String nume = rs.getString("Nume");
				String prenume = rs.getString("Prenume");
				Membru membru = getMemberByName(nume,prenume,obs);
				toReturn.add(membru);
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:getAllMembers " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	//-1 daca membrul sau cartea sunt prost date
	//-2 daca nu mai exista copie disponibila
	//-3 daca membrul nu are drept de rezervare
	//-4 daca membru are deja rezervate 3 carti
	// 0 la succes
	public int rezervareCarte(String nume, String prenume,String titlu){
		int toReturn = -1;
		CallableStatement statement = null;
		PreparedStatement helper = null;
		PreparedStatement helper2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		try{
			helper = dbConnection.prepareStatement(getMembru);
			helper.setString(1, nume);
			helper.setString(2, prenume);
			rs = helper.executeQuery();
			helper2 = dbConnection.prepareStatement(getTitlu);
			helper2.setString(1, titlu);
			rs2 = helper2.executeQuery();
			if(rs.next() && rs2.next()){
				CarteDAO carteDAO = new CarteDAOImpl();
				Carte carte = carteDAO.findByTitle(titlu);
				if(carte.getNrCopiiDisponibile() == 0){
					toReturn = -2;
				}
				else{
					Membru membru = getMemberByName(nume,prenume,false);
					if(membru.getDreptImprumut() == false){
						toReturn = -3;
					}
					else{
						if (membru.getImprumuturi().size() == 3){
							toReturn = -4;
						}
						else{
							toReturn = 0;
							statement = dbConnection.prepareCall(rezervare);
							statement.setString(1, nume);
							statement.setString(2, prenume);
							statement.setString(3, titlu);
							statement.execute();
						}
					}
				}
			}
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:rezervareCarte " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(statement);
			ConnectionFactory.close(helper);
			ConnectionFactory.close(rs);
		}
		return toReturn;
	}
	public int insert(Membru membru){
		PreparedStatement insertStatement= null;
		PreparedStatement insertStatementAddr = null;
		PreparedStatement insertStatementUser = null;
		PreparedStatement insertStatementValiditate = null;
		PreparedStatement insertStatementPenalizari = null;
		int insertedId = -1;
		try{
			insertStatement = dbConnection.prepareStatement(inserareMembru,Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, membru.getNume());
			insertStatement.setString(2, membru.getPrenume());
			insertStatement.setString(3, membru.getEmail());
			insertStatement.setString(4, membru.getNrTelefon());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next())
			insertedId = rs.getInt(1);
			
			insertStatementAddr = dbConnection.prepareStatement(inserareAdresa);
			insertStatementAddr.setInt(1, insertedId);
			insertStatementAddr.setString(2, membru.getAdresa().getStrada());
			insertStatementAddr.setInt(3, membru.getAdresa().getNumar());
			insertStatementAddr.setString(4, membru.getAdresa().getBloc());
			insertStatementAddr.setInt(5, membru.getAdresa().getApartament());
			insertStatementAddr.executeUpdate();
			
			insertStatementUser = dbConnection.prepareStatement(inserareUser);
			insertStatementUser.setString(1, membru.getEmail());
			insertStatementUser.setString(2, membru.getParola());
			insertStatementUser.executeUpdate();
			
			insertStatementPenalizari = dbConnection.prepareStatement(inserarePenalizari);
			insertStatementPenalizari.setInt(1, insertedId);
			insertStatementPenalizari.executeUpdate();
			
			insertStatementValiditate = dbConnection.prepareStatement(inserareValiditate);
			insertStatementValiditate.setInt(1, insertedId);
			insertStatementValiditate.executeUpdate();
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:insert " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(insertStatementAddr);
			ConnectionFactory.close(insertStatementUser);
			ConnectionFactory.close(insertStatementValiditate);
			ConnectionFactory.close(insertStatementPenalizari);
		}
		return insertedId;
	}
	public boolean update(Membru membru , String cust){
		PreparedStatement updateStatement= null;
		PreparedStatement updateStatementAddr = null;
		int id = -1;
		boolean validCustomer = true;
		try{
			Membru helper = getMemberByName(cust.split(" ")[0],cust.split(" ")[1],false);
			id = helper.getId();
			
			if (id != -1){
				updateStatement = dbConnection.prepareStatement(updateMembru);
				updateStatement.setString(1, membru.getNume());
				updateStatement.setString(2, membru.getPrenume());
				updateStatement.setString(3, membru.getEmail());
				updateStatement.setString(4, membru.getNrTelefon());
				updateStatement.setInt(5, id);
				updateStatement.executeUpdate();
				updateStatementAddr = dbConnection.prepareStatement(updateAdresa);
				updateStatementAddr.setString(1, membru.getAdresa().getStrada());
				updateStatementAddr.setInt(2, membru.getAdresa().getNumar());
				updateStatementAddr.setString(3, membru.getAdresa().getBloc());
				updateStatementAddr.setInt(4, membru.getAdresa().getApartament());
				updateStatementAddr.setInt(5, id);
				updateStatementAddr.executeUpdate();
			}
			else {
				validCustomer = false;
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "MembruDAO:update " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(updateStatementAddr);
		}
		return validCustomer;
	}
}
