package start;

import java.util.ArrayList;
import java.util.List;

import controller.LibraryController;
import dao.CarteDAO;
import dao.CarteDAOImpl;
import dao.MembruDAO;
import dao.MembruDAOImpl;
import model.Adresa;
import model.Carte;
import model.ImprumutRezervare;
import model.Membru;
import views.LibraryView;

public class Start {

	public static void main(String[] args) {
		LibraryView view = new LibraryView();
		LibraryController controller = new LibraryController(view);
		view.getMain().setVisible(true);
		MembruDAO membruDAO = new MembruDAOImpl();
		List<Membru> membrii = membruDAO.getAllMembers(true);
	}
}
